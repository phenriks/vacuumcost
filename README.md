# VacuumCOST
Code for simulating UHV chamber pressure and NEG saturation by iterative simulations of a Molflow model.


**Authors:** Peter Lindquist Henriksen, Marton Ady, Roberto Kersevan  
**Copyright:** CERN (2022)  

A simple example model is included that should run out of the box by simply running `Main_script.py`.

Prerequisites:
1. Molflow version 2.9.0 or later (https://cern.ch/molflow)  
2. Change the `path_to_molflowCLI` in the main script to point at the local Molflow CLI application.  

`Python_package_versions` lists the installed packages and their versions for a running version of the code.

## Quick guide for setting up a custom simulation
* Create a model in Molflow with desorping and absorbing facets and make sure it runs in the GUI without leaks.
* Save the model and put .xml file in the Iterations subfolder.
* Modify `Main_script.py` to choose which sticking model to use, as well as whether to run a fixed number of molecules or to simulate the system for a fixed amount of time.
* Define in `Main_script.py` which facets (by their initial sticking coefficient or facet IDs) that should have constant pumping speed throughout the simulation.
* More advanced settings include defining the scaling factor or fixed CPU time to run per iteration (controlling statistics vs. runtime), thresholds for maximum allowable change in sticking factor between two subsequent iterations, etc.
* Run the simulation simply by executing `Main_script.py`. VacuumCOST will save .xml files for each time slice in the Iterations subfolder and create plots and output files in the `Output` subfolder.

Changing or adding sticking models, modifying the injection of time steps, alterating considerations for handling the change in sticking factor across a subset of facets, etc. can be done in the `worker.py` file in the `Scripts` subfolder.\
In the `Process_and_plot_results.py` script, the user is highly encouraged to customize the output files and figures to be generated.

VacuumCOST can also be used to simulate sudden leaks, varying outgassing rates based on photo-stimulated desorption with the use of a photon flux map exported from Synrad, multiple PSD maps to simulate different materials in the same geometry with varying levels of conditioning, etc. A description of VacuumCOST along with a real world demonstrator case can be found in the article [here](https://doi.org/10.1016/j.vacuum.2023.111992 "VacuumCOST paper").

 
 ## Simulating outgassing based on dynamic PSD
 * Simulate the textured geometry with SynRad. Export the texture element area and flux (ph/s) facet by facet in SynRad.
 * When setting up the simulation in Molflow, select all facets to be assigned a certain material and assign them to a named selection group.
 * In `Main_script.py` selection desorption type 'Dynamic PSD from Synrad data' and point to the desorption yield data file to be used for each material selection group. Point as well to the data files containing exported texture cell area and flux information.