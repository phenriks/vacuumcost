# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 08:36:05 2022

@author: Peter Lindquist Henriksen
contact email 1: peter.lindquist.henriksen@cern.ch
contact email 2: peter@lindquist-henriksen.dk

"""

import numpy as np
import matplotlib                              # Matlab like syntax for linear algebra and functions
import matplotlib.pyplot as plt                        # Plots and figures like you know them from Matlab
# import xml.etree.ElementTree as ET
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.colors
from mpl_toolkits.mplot3d import Axes3D
import imageio
import os
from worker import store_abs_des,calculate_sticking,get_geometries,parse_molflow_file,get_facets_from_selection_group
import time
import datetime
from datetime import timedelta
import warnings

def Process_and_plot_NEG(file_to_run_name,save_iterations_folder,file_to_run_type,N_iter,Ndes_to_run,scaling,sticking_model,runtype,runtime,timesteps,plot_saturated,keep_aspect_ratio,switch_yz,N_abs,N_des,N_abs_before_scaling,N_des_before_scaling,run_simulation,saturated_area_cutoff,saturated_fraction_cutoff,run_processing,run_plotting,timesteps_tested,facet_inactive_critera_sticking,facet_inactive_critera_ID,facet_inactive_selection_group_names,saturation_limit,num_legends,plot_all_iterations,create_absorption_animation,create_sticking_animation,desorption_type,beam_current,z_res,sticking_change_calculation,create_pressure_animation,leak_facet):
    # Script to process and plot results of VacuumCOST simulations.
    # Users are highly encouraged to create their own plots suitable for their particular use case.
    
    print('Processing results started at ' + str(datetime.datetime.now()), flush = True)
    t0 = time.time()
    # %% Allocate arrays etc.
    file_to_run = save_iterations_folder + file_to_run_name + "_iter"+str(0) + file_to_run_type
    data = parse_molflow_file(file_to_run) # Load first iteration results
    
    N_facets = len(data.find('MolflowResults/Moments/Moment/FacetResults')[:])
    

    # Convert runtypes to timesteps (except for when running # of molecules)
    if runtype == 'By physical time elapsed exponential': # Convert anyway to timesteps
        runtype = 'By timesteps'
    elif runtype == 'By physical time elapsed linear': # Convert anyway to timesteps
        runtype = 'By timesteps'
    if runtype != 'By number of molecules':
        N_iter = len(timesteps)
        
    # If simulation has not run, need to load data from all iterations again. Only allocate empty arrays in this case
    if run_simulation!=1:
        N_abs = np.zeros([N_facets,N_iter])
        N_abs_before_scaling = np.zeros([N_facets,N_iter])
        N_des = np.zeros([N_facets,N_iter])
        N_des_before_scaling = np.zeros([N_facets,N_iter])  
    s = np.zeros([N_facets,N_iter]) #Sticking at beginning of iteration
    sticking_after = np.zeros([N_facets,N_iter]) #Sticking after the iteration has run
    P = np.zeros([N_facets,N_iter]) #pressure
    AC = np.zeros([N_facets,N_iter])
    active_facets = np.ones([N_facets,1]).flatten() #Start as ones for logical selection later
    area = np.zeros([N_facets,1])
    COM = np.zeros([N_facets,3])
    saturated = np.zeros([N_facets,N_iter])
    T = np.zeros([N_facets,1]).flatten()
    outgassing = np.zeros([N_facets,1]).flatten()
    outgassing_mbarls = np.zeros([N_facets,1]).flatten()
    Ndes_per_second = np.zeros([N_iter,1]).flatten()
    outgassing_rate = np.zeros([N_iter,1]).flatten()
    new_s = np.zeros([N_facets,1])
    Delta_s_rel = np.zeros([N_facets,1])
    
    T0 = 273.15 # 0 deg C in K
    N_A = 6.02214076e23 # Avogadro's constant (molecules/mol)
    initializing = False

        
    # %% Start loading data             
    CO_dens = 1  #Dummy variable, not used if sticking model does not need it.
    if sticking_model=='H2_CO_dependent':
        CO_dens = np.loadtxt('Output/CO_dens.csv', delimiter=',')
        
    # Load temperature of each facet
    for i in range(N_facets): # Loop through all facets
        T[i] = float(data.find('Geometry/Facets')[i].find('Temperature').attrib['value']) # Temperature of facet

    #Get geometries of facets
    area,COM = get_geometries(data,N_facets,area,COM)
    z_COM = COM[:,2] # z-coordinate of COM of all facets

    # Go through each run iteration
    for j in range(N_iter):
        run_iter = j
        print('Loading results for iteration ' + str(run_iter) + '/' + str(N_iter-1), flush=True)
        # Files to read in this iteration
        file_to_run = save_iterations_folder + file_to_run_name + "_iter"+str(run_iter) + file_to_run_type       
    
        #Load results from this iteration
        data = parse_molflow_file(file_to_run)
    
    
        # Looping through all facets and storing the results for facet i
        if run_simulation!=1: # If simulation not run, load all data again
            # Find the number of absorbed and desorbed (virtual) molecules from each facet before scaling
            N_abs_before_scaling = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,1)
            N_des_before_scaling = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,2)
            # Store the number of absorbed and desorbed (physical) molecules from each facet in this iteration after appropraite scaling
            N_abs = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,3)
            N_des = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,4)

        else: # Add just the last iteration of adsorbed and desorbed molecules
            if run_iter==N_iter-1:
                N_abs_before_scaling = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,1)
                N_des_before_scaling = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,2)
                N_abs = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,3)
                N_des = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,4)

        
      
        # Load outgassing - index shifted by 1 to store the outgassing *after* certain timestep
        if run_iter==0:
            for i in range(N_facets): # Loop through all facets
                if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['desType']) != 0: # If desorption type is not "None"
                    if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['useOutgassingFile']) == 1: # If outgassing comes from a file (dynamic)
                        outgassing_mbarls[i] = 10*float(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalOutgassing']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
                    else:
                        outgassing_mbarls[i] = 10*float(data.find('Geometry/Facets')[i].find('Outgassing').attrib['constValue']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
                else:
                    outgassing_mbarls[i] = 0
                outgassing_mols = outgassing_mbarls[i]*4.41e-5*(T0/T[i]) #mol/s # https://www.pfeiffer-vacuum.com/en/know-how/introduction-to-vacuum-technology/fundamentals/pv-throughput/
                outgassing[i] = outgassing_mols*N_A # molecules/s
            Ndes_per_second_initial = np.sum(outgassing) # molecules/s
            outgassing_rate_initial = np.sum(outgassing_mbarls) #mbar*l/s
        if run_iter!=0:
            for i in range(N_facets): # Loop through all facets
                if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['desType']) != 0: # If desorption type is not "None"
                    if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['useOutgassingFile']) == 1: # If outgassing comes from a file (dynamic)
                        outgassing_mbarls[i] = 10*float(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalOutgassing']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
                    else:
                        outgassing_mbarls[i] = 10*float(data.find('Geometry/Facets')[i].find('Outgassing').attrib['constValue']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
                else:
                    outgassing_mbarls[i] = 0
                outgassing_mols = outgassing_mbarls[i]*4.41e-5*(T0/T[i]) #mol/s # https://www.pfeiffer-vacuum.com/en/know-how/introduction-to-vacuum-technology/fundamentals/pv-throughput/
                outgassing[i] = outgassing_mols*N_A # molecules/s
            Ndes_per_second[run_iter-1] = np.sum(outgassing) # molecules/s
            outgassing_rate[run_iter-1] = np.sum(outgassing_mbarls) #mbar*l/s
            
                 
        # For calculating pressure
        N_des_virt = np.sum(N_des_before_scaling[:,j])
        # K is ratio of real to virtual molecules
        if run_iter==0:
            K = Ndes_per_second_initial/N_des_virt
        else:
            K = Ndes_per_second[run_iter-1]/N_des_virt
        
        
        for i in range(N_facets):
            # Load sticking coefficient before each iteration
            s[i,j] = data.find('Geometry/Facets')[i].find('Sticking').attrib['constValue']

            # Calculate pressure
            sum_v_ort = float(data.find('MolflowResults/Moments/Moment/FacetResults')[i].find('Hits').attrib['sum_v_ort'])
            gas_mass_molec = float(data.find('MolflowSimuSettings/Gas').attrib['mass'])
            gas_mass_kg = 1e-3*gas_mass_molec/N_A
            is2sided = int(data.find('Geometry/Facets')[i].find('Opacity').attrib['is2sided'])
            if is2sided: # Correct for 2-sidedness in pressure calculation
                sum_v_ort = sum_v_ort/2
            sum_I_ort = sum_v_ort*gas_mass_kg
            # From Pa to mbar and area in m^2
            P[i,j] = (1e-2)*sum_I_ort*K/(area[i]*1e-4) # Pressure on each facet after each iteration  
          
            
        
        if run_iter==0: # No reason to run computation for timeslices other than the first one
            # Determination of facets to be considered active (Boolean array). "Active" means having sticking coefficient updated in each iteration.
            for i in range(len(facet_inactive_critera_sticking)):
                active_facets = active_facets*(np.around(s[:,0],decimals=4)!=facet_inactive_critera_sticking[i])
            for i in range(len(facet_inactive_critera_ID)):
                active_facets[facet_inactive_critera_ID[i]-1] = 0 # -1 to translate from Molflow numbering (first facet has ID 1) to data file numbering (first facet has ID 0)
            for i in range(len(facet_inactive_selection_group_names)): # Loop through each named selection group
                active_facets[get_facets_from_selection_group(data,facet_inactive_selection_group_names[i])] = 0 # Make facets in named selection group inactive
            active_facets = active_facets==1 # Make boolean

        
        
        # Calculate the sticking after each iteration
        calc_stick_after = True
        new_s,Delta_s_rel,data,x0,start_stick = calculate_sticking(sticking_model,N_facets,active_facets,N_abs,run_iter,area,new_s,s,Delta_s_rel,sticking_change_calculation,data,AC,N_A,CO_dens,initializing,calc_stick_after)
        sticking_after[:,j] = new_s.flatten()
                
      
    
    # Load the outgassing after the last timestep
    file_to_run = save_iterations_folder + file_to_run_name + "_iter"+str(run_iter+1) + file_to_run_type       
    data = parse_molflow_file(file_to_run)
    for i in range(N_facets): # Loop through all facets
        if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['desType']) != 0: # If desorption type is not "None"
            if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['useOutgassingFile']) == 1: # If outgassing comes from a file (dynamic)
                outgassing_mbarls[i] = 10*float(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalOutgassing']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
            else:
                outgassing_mbarls[i] = 10*float(data.find('Geometry/Facets')[i].find('Outgassing').attrib['constValue']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
        else:
            outgassing_mbarls[i] = 0
        outgassing_mols = outgassing_mbarls[i]*4.41e-5*(T0/T[i]) #mol/s # https://www.pfeiffer-vacuum.com/en/know-how/introduction-to-vacuum-technology/fundamentals/pv-throughput/
        outgassing[i] = outgassing_mols*N_A # molecules/s
    Ndes_per_second[run_iter] = np.sum(outgassing) # molecules/s
    outgassing_rate[run_iter] = np.sum(outgassing_mbarls) #mbar*l/s
    
        
    
    
    
    # %% Finish
    
    # Save data to files
    np.savetxt('Output/Pressure_' + sticking_model + '.csv', P, delimiter=',') # Pressure
    np.savetxt('Output/Sticking_after_iterations.csv', sticking_after, delimiter=',') # Sticking factor as calculated after iteration n
    np.savetxt('Output/COM.csv', COM, delimiter=',') # Facet COM coordinates
    np.savetxt('Output/area.csv', area, delimiter=',') # Facet areas
    
    
    print('Loading complete. Processing...', flush=True)
    

    
    # Absorbed molecules in each section in each iteration
    N_abs_iter_sum = np.zeros([N_iter,1])
    N_abs_iter = np.zeros([N_facets,N_iter])
    for j in range(N_iter):
        for i in range(len(N_abs_iter)):
            N_abs_iter[i,j] = N_abs[i,j] - sum(N_abs_iter[i,0:j])
            N_abs_iter_sum[j] = np.sum(N_abs[:,j]) - np.sum(N_abs_iter_sum[0:j]) # Sum of absorbed molecules in each iteration

    
    # Make Boolean array of whether saturated or not
    # Not considered saturated if not relevant section
    for j in range(N_iter):
        if saturation_limit==0:
            if sticking_model=='H2_CO_dependent':
                saturated[active_facets,j] = CO_dens[active_facets,j]>=x0
            else:
                saturated[active_facets,j] = N_abs[active_facets,j]/area[active_facets,0]>=x0
        else:
            if sticking_model=='H2_CO_dependent':
                saturated[active_facets,j] = CO_dens[active_facets,j]>=saturation_limit
            else:
                saturated[active_facets,j] = N_abs[active_facets,j]/area[active_facets,0]>saturation_limit
    # Make True/False
    saturated = saturated == 1
    
    # Cut-off for minimum area of facets to be considered saturated
    if saturated_area_cutoff!=0:
        saturated[(area<saturated_area_cutoff).flatten(),:] = False

    # To avoid statistical artifacts with microfacets counting as saturated. Introduce lower limit for the area saturated to be considered true
    saturation_fraction_limit = saturated_fraction_cutoff # Some percentage of total facet area beyond "saturation_point" has to be saturated in order to be counted.
    x = np.arange(np.floor(min(z_COM)), np.ceil(max(z_COM)), 1).tolist() # Fine to just have 1 cm resolution in z for this
    x = np.array(x)
    # Point of saturation for each iteration
    saturation_point = np.zeros([N_iter,1])
    for j in range(N_iter):
        if sum(z_COM[saturated[:,j]])>0:
            saturation_point[j] = np.max(z_COM[saturated[:,j]][active_facets[saturated[:,j]]]) # Old version. Takes simply max z of any saturated facet regardless of area (can cause statistical artifacts)

            # Saturation point considered the point below which som fraction of the saturated facets are located
            # Min. some fraction of the saturated area has to exist beyond this point for it to be considered saturation point.
            if saturated_fraction_cutoff!=0:
                sat_area = np.zeros([len(x),1])
                for i in range(len(x)):
                    consider = z_COM[:]>=x[i]
                    sat_area[i] = sum(area[saturated[:,j]*consider]) # Take only facets that are both saturated and under consideration (adhering to z-lim)
                saturation_point[j] = max(x[sat_area[:].flatten()>=saturation_fraction_limit*sum(area[saturated[:,j]])])# The point (max z-coordiante) where the saturated area is more than the lower limit established

        else:
            saturation_point[j] = 0
            
    # Sticking at point of saturation
    if sticking_model=='CO_physics_model':
        if saturation_limit==0:
           sticking_saturated = 0
        else:
           sticking_saturated = start_stick*(1-(saturation_limit/x0))
    elif sticking_model=='H2_CO_dependent':
        if saturation_limit==0:
           sticking_saturated = 0
        else:
           sticking_saturated = start_stick*pow(1-(saturation_limit/x0),2)
           
    # Decide units to plot in based on temporal resolution of simulation
    if np.median(np.diff(timesteps)) <= 60:
        plot_unit = 's'
    elif np.median(np.diff(timesteps)) <= 60*60:
        plot_unit = 'min'
    elif np.median(np.diff(timesteps)) <= 60*60*24:
        plot_unit = 'hrs'
    else:
        plot_unit = 'days'
        
    # %% Plot results
    print('Processing results finished at ' + str(datetime.datetime.now()), flush = True)
    t1 = time.time()
    print('Time elapsed for loading and processing: ' + str(timedelta(seconds=(t1-t0))), flush = True)
    
    if run_plotting:
        print('Plotting results started at ' + str(datetime.datetime.now()), flush = True)
        t0 = time.time()
        # Reduce number of plotted legends (in case of many iterations)
        if num_legends>N_iter:
            num_legends = N_iter
        plot_legends = np.round(np.linspace(0, N_iter - 1, num_legends)).astype(int)
        


        # %%  Outgassing after each iteration
        fig0, ax0 = plt.subplots(figsize=(10, 8))
        ax0p1 = ax0.twinx()
        if sum(plot_legends==j)>0:
            if plot_unit == 's':
                ax0.plot(1,outgassing_rate_initial,'x',color='blue')
                ax0p1.plot(1,Ndes_per_second_initial,'x',color='blue')
                ax0.plot(timesteps,outgassing_rate,'x',color='blue')
                ax0p1.plot(timesteps,Ndes_per_second,'x',color='blue')
                ax0.set(xlabel = 'Time passed (s)')
            elif plot_unit == 'min':
                ax0.plot(1/60,outgassing_rate_initial,'x',color='blue')
                ax0p1.plot(1/60,Ndes_per_second_initial,'x',color='blue')
                ax0.plot(timesteps/60,outgassing_rate,'x',color='blue')
                ax0p1.plot(timesteps/60,Ndes_per_second,'x',color='blue')
                ax0.set(xlabel = 'Time passed (min)')
            elif plot_unit == 'hrs':
                ax0.plot(1/(60*60),outgassing_rate_initial,'x',color='blue')
                ax0p1.plot(1/(60*60),Ndes_per_second_initial,'x',color='blue')
                ax0.plot(timesteps/(60*60),outgassing_rate,'x',color='blue')
                ax0p1.plot(timesteps/(60*60),Ndes_per_second,'x',color='blue')
                ax0.set(xlabel = 'Time passed (hrs)')
            else:
                ax0.plot(1/(60*60*24),outgassing_rate_initial,'x',color='blue')
                ax0p1.plot(1/(60*60*24),Ndes_per_second_initial,'x',color='blue')
                ax0.plot(timesteps/(60*60*24),outgassing_rate,'x',color='blue')
                ax0p1.plot(timesteps/(60*60*24),Ndes_per_second,'x',color='blue')
                ax0.set(xlabel = 'Time passed (days)')
        elif plot_all_iterations:
                ax0.plot(1,outgassing_rate_initial,'x',color='blue')
                ax0p1.plot(1,Ndes_per_second_initial,'x',color='blue')
                ax0.plot(timesteps,outgassing_rate,'x',color='blue')
                ax0p1.plot(timesteps,Ndes_per_second,'x',color='blue')
                ax0.set(xlabel = 'Time passed (s)')
        if beam_current!=0: # Add secondary x-axis showing dose (Ah)
            ax0p2 = ax0.twiny()
            ax0p2.plot(1*beam_current/(60*60),outgassing_rate_initial,'x',color='blue') # Create a dummy plot
            ax0p2.plot(timesteps*beam_current/(60*60),outgassing_rate,'x',color='blue') # Create a dummy plot
            ax0p2.set_xscale('log')
            ax0p2.set_yscale('log')
            ax0p2.set(xlabel = 'Dose (Ah)')
        ax0.set(ylabel = 'Total outgassing rate (mbar*l/s)')
        ax0p1.set(ylabel = 'Total outgassing rate (molecules/s)')
        ax0.set_xscale('log')
        ax0p1.set_xscale('log')
        ax0.set_yscale('log')
        ax0p1.set_yscale('log')
        plt.tight_layout()
        plt.savefig('Output/Results_outgassing_rate.png', bbox_inches='tight')
        plt.close()
        if desorption_type == 'Dynamic PSD from Synrad data':
            print('Make sure you check file: Output/Results_outgassing_rate.png to ensure conditioning looks reasonable.', flush=True)




        # %%  Percentage of absorbed molecules in cm along z for each iteration       
        fig1, ax1 = plt.subplots(figsize=(10, 8))
        xx = np.linspace(0,max(z_COM),int(np.ceil(max(z_COM)/z_res))+1) # Evenly spaced in bins of z_res
        N_abs_z = np.zeros([len(xx),1]).flatten()
        for j in range(N_iter):
            for i in range(len(xx)-1):
                if i==0:
                    idx = np.where((z_COM[:]>=xx[i])*(z_COM[:]<=xx[i+1])) # Include 0 in the first bin
                else:
                    idx = np.where((z_COM[:]>xx[i])*(z_COM[:]<=xx[i+1]))
                idx = np.array(idx).flatten()
                N_abs_z[i] = 100*sum(N_abs_iter[idx,j])/N_abs_iter_sum[j]
            if sum(plot_legends==j)>0:
                if plot_unit == 's':
                    ax1.plot(xx,N_abs_z,'-',label=str(round(timesteps[j]))+' s')
                elif plot_unit == 'min':
                    ax1.plot(xx,N_abs_z,'-',label=str(round(timesteps[j]/60))+' min')
                elif plot_unit == 'hrs':
                    ax1.plot(xx,N_abs_z,'-',label=str(round(timesteps[j]/(60*60)))+' hrs')
                else:
                    ax1.plot(xx,N_abs_z,'-',label=str(round(timesteps[j]/(60*60*24)))+' days')
            elif plot_all_iterations:
                ax1.plot(xx,N_abs_z,'-')
        ax1.set(ylabel = 'Absorbed molecules (%)')
        ax1.set(xlabel = 'Distance along z (cm)')
        ax1.legend(loc=0)
        ax1.set_yscale('log')
        plt.tight_layout()
        plt.savefig('Output/Results_absorbed_molecules.png', bbox_inches='tight')
        plt.close()
                

        
        # %% Plot saturation for facets with min and max saturation
        
        fig3p5, ax3p5 = plt.subplots(figsize=(10, 5))
        idx_w = np.where(np.min(s[active_facets,-1])==(s[active_facets,-1]))[0][0]
        idx_b = np.where(np.max(s[active_facets,-1])==(s[active_facets,-1]))[0][0]
        sat_level_w = np.zeros([1,N_iter]).flatten()
        sat_level_b = np.zeros([1,N_iter]).flatten()
        for j in range(N_iter):
            if sticking_model=='H2_CO_dependent':
                sat_level_w[j] = CO_dens[active_facets][idx_w,j]/x0
                sat_level_b[j] = CO_dens[active_facets][idx_b,j]/x0
            else:
                sat_level_w[j] = (N_abs[active_facets][idx_w,j]/area[active_facets][idx_w,0])/x0
                sat_level_b[j] = (N_abs[active_facets][idx_b,j]/area[active_facets][idx_b,0])/x0
        if plot_unit == 's':
            ax3p5.plot(timesteps,sat_level_w,label='Most saturated')
            ax3p5.plot(timesteps,sat_level_b,label='Least saturated')
            ax3p5.set(xlabel = 'Time passed (s)')
        elif plot_unit == 'min':
            ax3p5.plot(timesteps/60,sat_level_w,label='Most saturated')
            ax3p5.plot(timesteps/60,sat_level_b,label='Least saturated')
            ax3p5.set(xlabel = 'Time passed (min)')
        elif plot_unit == 'hrs':
            ax3p5.set(xlabel = 'Time passed (hrs)')
            ax3p5.plot(timesteps/(60*60),sat_level_w,label='Most saturated')
            ax3p5.plot(timesteps/(60*60),sat_level_b,label='Least saturated')
        else:
            ax3p5.plot(timesteps/(24*60*60),sat_level_w,label='Most saturated')
            ax3p5.plot(timesteps/(24*60*60),sat_level_b,label='Least saturated')
            ax3p5.set(xlabel = 'Time passed (days)')     
        if sticking_model=='H2_CO_dependent':
            ax3p5.set(ylabel = 'Adsorbed CO/x0 (normalized)')
        else:
            ax3p5.set(ylabel = 'Adsorbed/x0 (normalized)')
        ax3p5.set_xscale('log')
        ax3p5.legend(loc=0)
        plt.tight_layout()
        plt.savefig('Output/Saturation_of_worst_facets_as_func_time.png', bbox_inches='tight')
        plt.close()
        
        # %% Plot timesteps considered for simulation
        fig3p6, ax3p6 = plt.subplots(figsize=(10, 8))
        x = range(len(timesteps_tested))
        if plot_unit == 's':
            ax3p6.plot(x,timesteps_tested,'-x')
            ax3p6.set(ylabel = 'Timestep tested (seconds)')
        elif plot_unit == 'min':
            ax3p6.plot(x,timesteps_tested/60,'-x')
            ax3p6.set(ylabel = 'Timestep tested (min)')
        elif plot_unit == 'hrs':
            ax3p6.plot(x,timesteps_tested/3600,'-x')
            ax3p6.set(ylabel = 'Timestep tested (hours)')
        else:
            ax3p6.plot(x,timesteps_tested/(3600*24),'-x')
            ax3p6.set(ylabel = 'Timestep tested (days)')
        ax3p6.set(xlabel = 'Iteration')
        ax3p6.set_yscale('log')
        plt.tight_layout()
        plt.savefig('Output/Timesteps_tested_in_simulation.png', bbox_inches='tight')
        plt.close()
        
        
        # %%  Coverage after each  iteration
        fig5, ax5 = plt.subplots(figsize=(10, 8))
        x = np.unique(z_COM[active_facets])
        theta = np.zeros([len(x)])
        plot_iterations = range(N_iter)
        for j in plot_iterations:
            for i in range(len(x)):
                theta[i] = (np.mean(s[COM[:,2]==x[i],0])-np.mean(sticking_after[COM[:,2]==x[i],j]))/np.mean(s[COM[:,2]==x[i],0])
            if sum(plot_legends==j)>0:
                if plot_unit == 's':
                    ax5.plot(x,theta,'-',label='Runtime '+ str(round(timesteps[j])) + ' s')
                elif plot_unit == 'min':
                    ax5.plot(x,theta,'-',label='Runtime '+ str(round(timesteps[j]/60)) + ' min')
                elif plot_unit == 'hrs':
                    ax5.plot(x,theta,'-',label='Runtime '+ str(round(timesteps[j]/3600)) + ' hrs')
                else:
                    ax5.plot(x,theta,'-',label='Runtime '+ str(round(timesteps[j]/(3600*24))) + ' days')
                    
            elif plot_all_iterations:
                ax5.plot(x,theta,'-')
            ax5.set(title = str(N_iter) + ' Iterations total')
        ax5.set(ylabel = 'Coverage (normalized)')
        ax5.set(xlabel = 'Distance along z (cm)')
        ax5.legend(loc=0)
        ax5.set_yscale('log')
        plt.tight_layout()
        ax5.tick_params(top=True, right=True)
        # plt.grid()a
        plt.savefig('Output/Results_coverage.png', bbox_inches='tight')
        plt.close()
        
        # %% Plot 2D images
        xx = np.linspace(0,max(z_COM),int(np.ceil(max(z_COM)/z_res))+1) # Evenly spaced in bins of z_res
        columns_sticking_after = np.zeros([len(xx)-1,N_iter])
        columns_N_abs = np.zeros([len(xx)-1,N_iter])
        columns_N_abs_iter = np.zeros([len(xx)-1,N_iter])   
        warnings.simplefilter("ignore") # Ignore warnings on e.g. missing contours if no saturation
    
        # Calculating 2D arrays
        for j in range(N_iter):
            for i in range(len(xx)-1):
                if i==0:
                    idx = np.where((z_COM[:]>=xx[i])*(z_COM[:]<=xx[i+1])) # Include 0 in the first bin
                else:
                    idx = np.where((z_COM[:]>xx[i])*(z_COM[:]<=xx[i+1]))
                idx = np.array(idx).flatten()
                columns_sticking_after[i,j] = np.mean(sticking_after[idx,j][active_facets[idx]]) # Only show sticking of facets that are "active" as these are the only ones that have interesting sticking evolution
                columns_N_abs[i,j] = np.mean(N_abs[idx,j]) # Show absorption from all facets including those with fixed sticking.
                columns_N_abs_iter[i,j] = np.mean(N_abs_iter[idx,j])
        
        num_x_ticks = 6
        
        
        fig6, ax6 = plt.subplots(figsize=(10, 8))
        im6 = ax6.imshow(columns_sticking_after, aspect='auto', interpolation='none')
        
        # Create proper yticklabels        
        y_tick_list = ax6.get_yticks()
        y_label_list = y_tick_list*z_res
        ax6.set_yticklabels(y_label_list.astype(int))
        
        im6.set_clim(0.0, np.max(np.max(s[active_facets])))
        
        cbar6 = fig6.colorbar(im6)
        cbar6.set_label('Mean sticking')
        cbar6.ax.axhline(y=sticking_saturated, c='r') # Indicate where considered saturated
        
        # Set number of ticks
        tick_locator = matplotlib.ticker.MaxNLocator(nbins=6)
        cbar6.locator = tick_locator
        cbar6.update_ticks()
        # It adds one extra (off scale) so remove that
        original_ticks = list(np.round(cbar6.get_ticks(),3))
        original_ticks = original_ticks[0:-1]
        # Add ticks for saturation point and max sticking
        cbar6.set_ticks(original_ticks + [sticking_saturated] + [max(sticking_after[active_facets,0])])
        # Modify label for saturation point
        new_ticklabels = list(np.round(cbar6.get_ticks(),3))
        new_ticklabels[len(new_ticklabels)-2] = 'Saturation Point'
        # Set new labels
        cbar6.set_ticklabels(new_ticklabels)
        

        ax6.set(ylabel = 'Distance along z (cm)')
        if plot_unit == 's':
            ax6.set(xlabel = 'Time (seconds)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]])))
        elif plot_unit == 'min':
            ax6.set(xlabel = 'Time (minutes)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/60)))
        elif plot_unit == 'hrs':
            ax6.set(xlabel = 'Time (hours)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/3600)))
        else:
            ax6.set(xlabel = 'Time (days)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/(3600*24))))

        if leak_facet.size>0:
            N_leaks = len(leak_facet[:,0])
            for i in range(N_leaks):
                # Add tick for timestep just before leak occurs
                xticks = np.append(xticks,np.min(np.where(timesteps[:]>=leak_facet[i,1])))   
                if plot_unit == 's':
                    xticklabels.append(str(round(leak_facet[i,1])))
                elif plot_unit == 'min':
                    xticklabels.append(str(round(leak_facet[i,1]/60)))
                elif plot_unit == 'hrs':
                    xticklabels.append(str(round(leak_facet[i,1]/3600))) 
                else:
                    xticklabels.append(str(round(leak_facet[i,1]/(3600*24))))  
                    
        ax6.set(xticks = xticks)
        ax6.set(xticklabels = xticklabels)
        
        if beam_current!=0: # Add secondary x-axis showing dose (Ah)
            ax6p1 = ax6.twiny()
            ax6p1.imshow(columns_sticking_after, aspect='auto', interpolation='none')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]*beam_current/(60*60))))
            if leak_facet.size>0:
                for i in range(N_leaks):
                    # Add tick for timestep just before leak occurs
                    xticks = np.append(xticks,np.min(np.where(timesteps[:]>=leak_facet[i,1])))   
                    xticklabels.append(str(round(beam_current*leak_facet[i,1]/3600)))
            ax6p1.set(xlabel = 'Dose (Ah)')
            ax6p1.set(xticks = xticks)
            ax6p1.set(xticklabels = xticklabels)
            y_tick_list = ax6p1.get_yticks()
            y_label_list = y_tick_list*z_res
            ax6p1.set_yticklabels(y_label_list.astype(int))
        
        plt.contour(columns_sticking_after, [sticking_saturated], colors=['red'])
        if leak_facet.size>0:
            for i in range(N_leaks):
                plt.axvline(x=np.min(np.where(timesteps[:]>=leak_facet[i,1])),color='black',linestyle='--')


        plt.savefig('Output/Results_sticking_2D.png', bbox_inches='tight')
        plt.close()
        
        fig6, ax6 = plt.subplots(figsize=(10, 8))
        cmap = plt.cm.get_cmap("viridis")
        norm = matplotlib.colors.SymLogNorm(linthresh=np.min(columns_N_abs[np.where(columns_N_abs>0)]), vmin=0, vmax=np.max(np.max(columns_N_abs)))
        sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
        sm.set_array([])
        im6 = ax6.imshow(columns_N_abs,cmap=cmap,norm=norm, aspect='auto', interpolation='none')
        # Create proper yticklabels        
        y_tick_list = ax6.get_yticks()
        y_label_list = y_tick_list*z_res
        ax6.set_yticklabels(y_label_list.astype(int))
        
        cbar6 = fig6.colorbar(im6)
        ax6.set(ylabel = 'Distance along z (cm)')
        if plot_unit == 's':
            ax6.set(xlabel = 'Time (seconds)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]])))
        elif plot_unit == 'min':
            ax6.set(xlabel = 'Time (minutes)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/60)))
        elif plot_unit == 'hrs':
            ax6.set(xlabel = 'Time (hours)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/3600)))
        else:
            ax6.set(xlabel = 'Time (days)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/(3600*24))))
                
        if leak_facet.size>0:
            N_leaks = len(leak_facet[:,0])
            for i in range(N_leaks):
                # Add tick for timestep just before leak occurs
                xticks = np.append(xticks,np.min(np.where(timesteps[:]>=leak_facet[i,1])))   
                if plot_unit == 's':
                    xticklabels.append(str(round(leak_facet[i,1])))
                elif plot_unit == 'min':
                    xticklabels.append(str(round(leak_facet[i,1]/60)))
                elif plot_unit == 'hrs':
                    xticklabels.append(str(round(leak_facet[i,1]/3600))) 
                else:
                    xticklabels.append(str(round(leak_facet[i,1]/(3600*24))))  
                    
        ax6.set(xticks = xticks)
        ax6.set(xticklabels = xticklabels)
        cbar6.set_label('Total # of adsorbed molecules')
        
        if beam_current!=0: # Add secondary x-axis showing dose (Ah)
            ax6p1 = ax6.twiny()
            ax6p1.imshow(columns_N_abs,cmap=cmap,norm=norm, aspect='auto', interpolation='none')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]*beam_current/(60*60))))
            if leak_facet.size>0:
                for i in range(N_leaks):
                    # Add tick for timestep just before leak occurs
                    xticks = np.append(xticks,np.min(np.where(timesteps[:]>=leak_facet[i,1])))   
                    xticklabels.append(str(round(beam_current*leak_facet[i,1]/3600)))
            ax6p1.set(xlabel = 'Dose (Ah)')
            ax6p1.set(xticks = xticks)
            ax6p1.set(xticklabels = xticklabels)
            y_tick_list = ax6p1.get_yticks()
            y_label_list = y_tick_list*z_res
            ax6p1.set_yticklabels(y_label_list.astype(int))
        
        if leak_facet.size>0:
            for i in range(N_leaks):
                plt.axvline(x=np.min(np.where(timesteps[:]>=leak_facet[i,1])),color='red',linestyle='--')

        plt.savefig('Output/Results_adsorbed_total_2D.png', bbox_inches='tight')
        plt.close()
        
        fig6, ax6 = plt.subplots(figsize=(10, 8))
        cmap = plt.cm.get_cmap("viridis")
        norm = matplotlib.colors.SymLogNorm(linthresh=np.min(columns_N_abs_iter[np.where(columns_N_abs_iter>0)]), vmin=0, vmax=np.max(np.max(columns_N_abs_iter)))
        sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
        sm.set_array([])
        im6 = ax6.imshow(columns_N_abs_iter,cmap=cmap,norm=norm, aspect='auto', interpolation='none')
        # Create proper yticklabels        
        y_tick_list = ax6.get_yticks()
        y_label_list = y_tick_list*z_res
        ax6.set_yticklabels(y_label_list.astype(int))
        
        cbar6 = fig6.colorbar(im6)
        ax6.set(ylabel = 'Distance along z (cm)')
        if plot_unit == 's':
            ax6.set(xlabel = 'Time (seconds)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]])))
        elif plot_unit == 'min':
            ax6.set(xlabel = 'Time (minutes)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/60)))
        elif plot_unit == 'hrs':
            ax6.set(xlabel = 'Time (hours)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/3600)))
        else:
            ax6.set(xlabel = 'Time (days)')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]/(3600*24))))
                
        if leak_facet.size>0:
            N_leaks = len(leak_facet[:,0])
            for i in range(N_leaks):
                # Add tick for timestep just before leak occurs
                xticks = np.append(xticks,np.min(np.where(timesteps[:]>=leak_facet[i,1])))   
                if plot_unit == 's':
                    xticklabels.append(str(round(leak_facet[i,1])))
                elif plot_unit == 'min':
                    xticklabels.append(str(round(leak_facet[i,1]/60)))
                elif plot_unit == 'hrs':
                    xticklabels.append(str(round(leak_facet[i,1]/3600))) 
                else:
                    xticklabels.append(str(round(leak_facet[i,1]/(3600*24))))  
                    
        ax6.set(xticks = xticks)
        ax6.set(xticklabels = xticklabels)
        cbar6.set_label('# of adsorbed molecules in iteration')
        if beam_current!=0: # Add secondary x-axis showing dose (Ah)
            ax6p1 = ax6.twiny()
            ax6p1.imshow(columns_N_abs_iter,cmap=cmap,norm=norm, aspect='auto', interpolation='none')
            xticklabels = []
            xticks = np.round(np.linspace(0, len(timesteps) - 1, num_x_ticks)).astype(int)
            for i in range(len(xticks)):
                xticklabels.append(str(round(timesteps[xticks[i]]*beam_current/(60*60))))
            if leak_facet.size>0:
                for i in range(N_leaks):
                    # Add tick for timestep just before leak occurs
                    xticks = np.append(xticks,np.min(np.where(timesteps[:]>=leak_facet[i,1])))   
                    xticklabels.append(str(round(beam_current*leak_facet[i,1]/3600)))
            ax6p1.set(xlabel = 'Dose (Ah)')
            ax6p1.set(xticks = xticks)
            ax6p1.set(xticklabels = xticklabels)
            y_tick_list = ax6p1.get_yticks()
            y_label_list = y_tick_list*z_res
            ax6p1.set_yticklabels(y_label_list.astype(int))
            
        if leak_facet.size>0:
            for i in range(N_leaks):
                plt.axvline(x=np.min(np.where(timesteps[:]>=leak_facet[i,1])),color='red',linestyle='--')
                
        plt.savefig('Output/Results_adsorbed_iteration_2D.png', bbox_inches='tight')
        plt.close()
    
        # %% Make GIF of absorption
    
        # Define colormap and normalization
        cmap = plt.cm.get_cmap("viridis")
        norm = matplotlib.colors.SymLogNorm(linthresh=1, vmin=0, vmax=max(max(N_abs[active_facets,N_iter-1]/area[active_facets].T)))
        sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
        sm.set_array([])
        
        xmin = max(COM[:,0])
        ymin = max(COM[:,1])
        zmin = max(COM[:,2])
        xmax = min(COM[:,0])
        ymax = min(COM[:,1])
        zmax = min(COM[:,2])
        
        filenames = []
        
        # Build geometry
        facets = []
        fig7a = plt.figure(figsize=(10, 8))
        ax7a = fig7a.add_subplot(1, 1, 1, projection=Axes3D.name)
        counter = -1
        for i in range(N_facets): # Loop through all facets
            if active_facets[i]:
                counter = counter+1
                Num_v = len(data.find('Geometry/Facets')[i].find('Indices')[:])
                vertices = np.zeros([Num_v,4])
                for k in range(len(data.find('Geometry/Facets')[i].find('Indices')[:])): # Loop through all verticies for given facet
                    vertices[k,0] = int(data.find('Geometry/Facets')[i].find('Indices')[k].attrib['vertex']) # Vertex ID
                    vertices[k,1] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['x'])
                    vertices[k,2] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['y'])
                    vertices[k,3] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['z'])
                    
                x = vertices[:,1]
                y = vertices[:,2]
                z = vertices[:,3]
                if switch_yz:
                    verticesss = [list(zip(x,z,y))]
                else:
                    verticesss = [list(zip(x,y,z))]
                # Create facet for this iteration and add to collection
                poly = Poly3DCollection(verticesss,color=cmap(norm(N_abs[i,j]/area[i].T)))
                facets.append(poly) # Save each facet polygon for later updating of colors
                ax7a.add_collection3d(poly)
                
                #Update axis limits based on comparing current limits with coordinates from this iteration
                if xmin>min(x):
                    xmin = min(x)
                if ymin>min(y):
                    ymin = min(y)
                if zmin>min(z):
                    zmin = min(z)
                if xmax<max(x):
                    xmax = max(x)
                if ymax<max(y):
                    ymax = max(y)
                if zmax<max(z):
                    zmax = max(z)
        
        # Set up nice plot
        if keep_aspect_ratio:
            # Create cubic bounding box to simulate equal aspect ratio
            max_range = np.array([xmax-xmin, ymax-ymin, zmax-zmin]).max()
            Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(xmax+xmin)
            Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(ymax+ymin)
            Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(zmax+zmin)
            for xb, yb, zb in zip(Xb, Yb, Zb):
                if switch_yz == 1:
                    ax7a.plot([xb], [zb], [yb], 'w')
                else:
                    ax7a.plot([xb], [yb], [zb], 'w')
        elif keep_aspect_ratio == False:
            ax7a.set_xlim(xmin,xmax)
            if switch_yz:
                ax7a.set_zlim(ymin,ymax)
                ax7a.set_ylim(zmin,zmax)
            else:
                ax7a.set_ylim(ymin,ymax)
                ax7a.set_zlim(zmin,zmax)
        else:
            print('No valid choice made for aspect ratio', flush=True)
    
        ax7a.set_xlabel('x position (cm)')
        if switch_yz:
            ax7a.set_zlabel('y position (cm)')
            ax7a.set_ylabel('z position (cm)')
        else:
            ax7a.set_ylabel('y position (cm)')
            ax7a.set_zlabel('z position (cm)')
        cbar2 = fig7a.colorbar(sm)
        cbar2.set_label('Density of adbsorbed molecules (molec/cm$^{2}$)')
        
        cbar2.ax.axhline(y=x0, c='r') # Indicate where considered saturated

        
        ax7a.view_init(30,-20)
        # ax7a.view_init(20,0)
        plt.tight_layout()
        if create_absorption_animation:
            
            print('Creating absorption animation...', flush = True)

            # Update colors in each iteration
            for j in range(N_iter):
                counter = -1
                for i in range(N_facets): # Loop through all facets
                    if active_facets[i]: # Only update colors of active facets
                        counter = counter+1
                        facets[counter].set_color(cmap(norm(N_abs[i,j]/area[i].T)))
                        if saturated[i,j]:
                            if plot_saturated:
                                facets[counter].set_edgecolor('red')
                                
                if beam_current!=0: # Add conditioning time in Ah
                    if plot_unit == 's':
                        plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    elif plot_unit == 'min':
                        plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    elif plot_unit == 'hrs':
                        plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    else:
                        plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                else:
                    if plot_unit == 's':
                        plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds')
                    elif plot_unit == 'min':
                        plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes')
                    elif plot_unit == 'hrs':
                        plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours')
                    else:
                        plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days')
        
                # create file name and append it to a list
                filename = f'{j}.png'
                filenames.append(filename)
                plt.savefig('Output/'+filename, bbox_inches='tight')
                if j==N_iter-1: # Save image of final status of the simulation
                    plt.savefig('Output/Results_3D_final_iteration_absorption.png', bbox_inches='tight')
            plt.close()
            
            # build gif
            with imageio.get_writer('Output/NEG_saturation_animation_absorption.gif', mode='I') as writer:
                for filename in filenames:
                    image = imageio.v2.imread('Output/'+filename)
                    writer.append_data(image)
                    
            # Remove files
            for filename in set(filenames):
                os.remove('Output/'+filename)
    
        else: # Just save image of final status of the simulation
            j = N_iter-1
            counter = -1
            for i in range(N_facets): # Loop through all facets
                if active_facets[i]: # Only update colors of active facets
                    counter = counter+1
                    facets[counter].set_color(cmap(norm(N_abs[i,j]/area[i].T)))
                    if saturated[i,j]:
                        if plot_saturated:
                            facets[counter].set_edgecolor('red')
                            
            if beam_current!=0: # Add conditioning time in Ah
                if plot_unit == 's':
                    plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                elif plot_unit == 'min':
                    plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                elif plot_unit == 'hrs':
                    plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                else:
                    plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
            else:
                if plot_unit == 's':
                    plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds')
                elif plot_unit == 'min':
                    plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes')
                elif plot_unit == 'hrs':
                    plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours')
                else:
                    plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days')
    
            plt.savefig('Output/Results_3D_final_iteration_absorption.png', bbox_inches='tight')
            plt.close()



        # %% Make GIF of sticking
    
        # Define colormap and normalization
        cmap = plt.cm.get_cmap("viridis")
        norm = matplotlib.colors.SymLogNorm(linthresh=1,vmin=0, vmax=max(sticking_after[active_facets,0]))
        sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
        sm.set_array([])
        
        
        xmin = max(COM[:,0])
        ymin = max(COM[:,1])
        zmin = max(COM[:,2])
        xmax = min(COM[:,0])
        ymax = min(COM[:,1])
        zmax = min(COM[:,2])
        
        filenames = []
        
        # Build geometry
        facets = []
        fig7b = plt.figure(figsize=(10, 8))
        ax7b = fig7b.add_subplot(1, 1, 1, projection=Axes3D.name)
        counter = -1
        for i in range(N_facets): # Loop through all facets
            if active_facets[i]:
                counter = counter+1
                Num_v = len(data.find('Geometry/Facets')[i].find('Indices')[:])
                vertices = np.zeros([Num_v,4])
                for k in range(len(data.find('Geometry/Facets')[i].find('Indices')[:])): # Loop through all verticies for given facet
                    vertices[k,0] = int(data.find('Geometry/Facets')[i].find('Indices')[k].attrib['vertex']) # Vertex ID
                    vertices[k,1] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['x'])
                    vertices[k,2] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['y'])
                    vertices[k,3] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['z'])
                    
                x = vertices[:,1]
                y = vertices[:,2]
                z = vertices[:,3]
                if switch_yz:
                    verticesss = [list(zip(x,z,y))]
                else:
                    verticesss = [list(zip(x,y,z))]
                # Create facet for this iteration and add to collection
                poly = Poly3DCollection(verticesss,color=cmap(norm(N_abs[i,j]/area[i].T)))
                facets.append(poly) # Save each facet polygon for later updating of colors
                ax7b.add_collection3d(poly)
                
                #Update axis limits based on comparing current limits with coordinates from this iteration
                if xmin>min(x):
                    xmin = min(x)
                if ymin>min(y):
                    ymin = min(y)
                if zmin>min(z):
                    zmin = min(z)
                if xmax<max(x):
                    xmax = max(x)
                if ymax<max(y):
                    ymax = max(y)
                if zmax<max(z):
                    zmax = max(z)
        
        # Set up nice plot
        if keep_aspect_ratio:
            # Create cubic bounding box to simulate equal aspect ratio
            max_range = np.array([xmax-xmin, ymax-ymin, zmax-zmin]).max()
            Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(xmax+xmin)
            Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(ymax+ymin)
            Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(zmax+zmin)
            for xb, yb, zb in zip(Xb, Yb, Zb):
                if switch_yz == 1:
                    ax7b.plot([xb], [zb], [yb], 'w')
                else:
                    ax7b.plot([xb], [yb], [zb], 'w')
        elif keep_aspect_ratio == False:
            ax7b.set_xlim(xmin,xmax)
            if switch_yz:
                ax7b.set_zlim(ymin,ymax)
                ax7b.set_ylim(zmin,zmax)
            else:
                ax7b.set_ylim(ymin,ymax)
                ax7b.set_zlim(zmin,zmax)
        else:
            print('No valid choice made for aspect ratio', flush=True)
    
        ax7b.set_xlabel('x position (cm)')
        if switch_yz:
            ax7b.set_zlabel('y position (cm)')
            ax7b.set_ylabel('z position (cm)')
        else:
            ax7b.set_ylabel('y position (cm)')
            ax7b.set_zlabel('z position (cm)')
        cbar2 = fig7b.colorbar(sm)

        # Colorbar ticks and ticklabels
        cbar2.set_label('Sticking factor')
        cbar2.ax.axhline(y=sticking_saturated, c='r') # Indicate where considered saturated
        
        # Set number of ticks
        tick_locator = matplotlib.ticker.MaxNLocator(nbins=6)
        cbar2.locator = tick_locator
        cbar2.update_ticks()
        # It adds one extra (off scale) so remove that
        original_ticks = list(np.round(cbar2.get_ticks(),3))
        original_ticks = original_ticks[0:-1]
        # Add ticks for saturation point and max sticking
        cbar2.set_ticks(original_ticks + [sticking_saturated] + [max(sticking_after[active_facets,0])])
        # Modify label for saturation point
        new_ticklabels = list(np.round(cbar2.get_ticks(),3))
        new_ticklabels[len(new_ticklabels)-2] = 'Saturation Point'
        # Set new labels
        cbar2.set_ticklabels(new_ticklabels)
        

        
        ax7b.view_init(30,-20)
        # ax7b.view_init(20,0)
        plt.tight_layout()
        if create_sticking_animation:
            
            print('Creating sticking animation...', flush = True)

            # Update colors in each iteration
            for j in range(N_iter):
                counter = -1
                for i in range(N_facets): # Loop through all facets
                    if active_facets[i]: # Only update colors of active facets
                        counter = counter+1
                        facets[counter].set_color(cmap(norm(sticking_after[i,j])))
                        if saturated[i,j]:
                            if plot_saturated:
                                facets[counter].set_edgecolor('red')
                                
                if beam_current!=0: # Add conditioning time in Ah
                    if plot_unit == 's':
                        plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    elif plot_unit == 'min':
                        plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    elif plot_unit == 'hrs':
                        plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    else:
                        plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                else:
                    if plot_unit == 's':
                        plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds')
                    elif plot_unit == 'min':
                        plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes')
                    elif plot_unit == 'hrs':
                        plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours')
                    else:
                        plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days')
        
                # create file name and append it to a list
                filename = f'{j}.png'
                filenames.append(filename)
                plt.savefig('Output/'+filename, bbox_inches='tight')
                if j==N_iter-1: # Save image of final status of the simulation
                    plt.savefig('Output/Results_3D_final_iteration_sticking.png', bbox_inches='tight')
                    ax7b.view_init(20,-60)
                    plt.savefig('Output/Results_3D_final_iteration_sticking2.png', bbox_inches='tight')
            plt.close()
            
            # build gif
            with imageio.get_writer('Output/NEG_saturation_animation_sticking.gif', mode='I') as writer:
                for filename in filenames:
                    image = imageio.v2.imread('Output/'+filename)
                    writer.append_data(image)
                    
            # Remove files
            for filename in set(filenames):
                os.remove('Output/'+filename)
    
        else: # Just save image of final status of the simulation
            j = N_iter-1
            counter = -1
            for i in range(N_facets): # Loop through all facets
                if active_facets[i]: # Only update colors of active facets
                    counter = counter+1
                    facets[counter].set_color(cmap(norm(sticking_after[i,j])))
                    if saturated[i,j]:
                        if plot_saturated:
                            facets[counter].set_edgecolor('red')
                            
            if beam_current!=0: # Add conditioning time in Ah
                if plot_unit == 's':
                    plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                elif plot_unit == 'min':
                    plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                elif plot_unit == 'hrs':
                    plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                else:
                    plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
            else:
                if plot_unit == 's':
                    plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds')
                elif plot_unit == 'min':
                    plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes')
                elif plot_unit == 'hrs':
                    plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours')
                else:
                    plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days')
    
            plt.savefig('Output/Results_3D_final_iteration_sticking.png', bbox_inches='tight')
            plt.close()


        # %% Make GIF of pressure
    
        # Define colormap and normalization
        cmap = plt.cm.get_cmap("viridis")
        norm = matplotlib.colors.LogNorm(vmin=np.min(P[P>0]), vmax=np.max(P))
        sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
        sm.set_array([])
        
        
        xmin = max(COM[:,0])
        ymin = max(COM[:,1])
        zmin = max(COM[:,2])
        xmax = min(COM[:,0])
        ymax = min(COM[:,1])
        zmax = min(COM[:,2])
        
        filenames = []
        
        # Build geometry
        facets = []
        fig7b = plt.figure(figsize=(10, 8))
        ax7b = fig7b.add_subplot(1, 1, 1, projection=Axes3D.name)
        counter = -1
        for i in range(N_facets): # Loop through all facets
            if active_facets[i]:
                counter = counter+1
                Num_v = len(data.find('Geometry/Facets')[i].find('Indices')[:])
                vertices = np.zeros([Num_v,4])
                for k in range(len(data.find('Geometry/Facets')[i].find('Indices')[:])): # Loop through all verticies for given facet
                    vertices[k,0] = int(data.find('Geometry/Facets')[i].find('Indices')[k].attrib['vertex']) # Vertex ID
                    vertices[k,1] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['x'])
                    vertices[k,2] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['y'])
                    vertices[k,3] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['z'])
                    
                x = vertices[:,1]
                y = vertices[:,2]
                z = vertices[:,3]
                if switch_yz:
                    verticesss = [list(zip(x,z,y))]
                else:
                    verticesss = [list(zip(x,y,z))]
                # Create facet for this iteration and add to collection
                poly = Poly3DCollection(verticesss,color=cmap(norm(P[i,j])))
                facets.append(poly) # Save each facet polygon for later updating of colors
                ax7b.add_collection3d(poly)
                
                #Update axis limits based on comparing current limits with coordinates from this iteration
                if xmin>min(x):
                    xmin = min(x)
                if ymin>min(y):
                    ymin = min(y)
                if zmin>min(z):
                    zmin = min(z)
                if xmax<max(x):
                    xmax = max(x)
                if ymax<max(y):
                    ymax = max(y)
                if zmax<max(z):
                    zmax = max(z)
        
        # Set up nice plot
        if keep_aspect_ratio:
            # Create cubic bounding box to simulate equal aspect ratio
            max_range = np.array([xmax-xmin, ymax-ymin, zmax-zmin]).max()
            Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(xmax+xmin)
            Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(ymax+ymin)
            Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(zmax+zmin)
            for xb, yb, zb in zip(Xb, Yb, Zb):
                if switch_yz == 1:
                    ax7b.plot([xb], [zb], [yb], 'w')
                else:
                    ax7b.plot([xb], [yb], [zb], 'w')
        elif keep_aspect_ratio == False:
            ax7b.set_xlim(xmin,xmax)
            if switch_yz:
                ax7b.set_zlim(ymin,ymax)
                ax7b.set_ylim(zmin,zmax)
            else:
                ax7b.set_ylim(ymin,ymax)
                ax7b.set_zlim(zmin,zmax)
        else:
            print('No valid choice made for aspect ratio', flush=True)
    
        ax7b.set_xlabel('x position (cm)')
        if switch_yz:
            ax7b.set_zlabel('y position (cm)')
            ax7b.set_ylabel('z position (cm)')
        else:
            ax7b.set_ylabel('y position (cm)')
            ax7b.set_zlabel('z position (cm)')
        cbar2 = fig7b.colorbar(sm)
        cbar2.set_label('Pressure (mbar)')
        
        ax7b.view_init(30,-20)
        # ax7b.view_init(20,0)
        plt.tight_layout()
        if create_pressure_animation:
            
            print('Creating pressure animation...', flush = True)

            # Update colors in each iteration
            for j in range(N_iter):
                counter = -1
                for i in range(N_facets): # Loop through all facets
                    if active_facets[i]: # Only update colors of active facets
                        counter = counter+1
                        facets[counter].set_color(cmap(norm(P[i,j])))
                                
                if beam_current!=0: # Add conditioning time in Ah
                    if plot_unit == 's':
                        plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    elif plot_unit == 'min':
                        plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    elif plot_unit == 'hrs':
                        plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                    else:
                        plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                else:
                    if plot_unit == 's':
                        plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds')
                    elif plot_unit == 'min':
                        plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes')
                    elif plot_unit == 'hrs':
                        plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours')
                    else:
                        plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days')
        
                # create file name and append it to a list
                filename = f'{j}.png'
                filenames.append(filename)
                plt.savefig('Output/'+filename, bbox_inches='tight')
                if j==N_iter-1: # Save image of final status of the simulation
                    plt.savefig('Output/Results_3D_final_iteration_pressure.png', bbox_inches='tight')
                    ax7b.view_init(20,-60)
                    plt.savefig('Output/Results_3D_final_iteration_pressure2.png', bbox_inches='tight')
            plt.close()
            
            # build gif
            with imageio.get_writer('Output/NEG_saturation_animation_pressure.gif', mode='I') as writer:
                for filename in filenames:
                    image = imageio.v2.imread('Output/'+filename)
                    writer.append_data(image)
                    
            # Remove files
            for filename in set(filenames):
                os.remove('Output/'+filename)
    
        else: # Just save image of final status of the simulation
            j = N_iter-1
            counter = -1
            for i in range(N_facets): # Loop through all facets
                if active_facets[i]: # Only update colors of active facets
                    counter = counter+1
                    facets[counter].set_color(cmap(norm(P[i,j])))
                            
            if beam_current!=0: # Add conditioning time in Ah
                if plot_unit == 's':
                    plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                elif plot_unit == 'min':
                    plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                elif plot_unit == 'hrs':
                    plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
                else:
                    plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days - ' + str(round(timesteps[j]*beam_current/(60*60))) + '/' + str(round(timesteps[-1]*beam_current/(60*60))) + ' Ah')
            else:
                if plot_unit == 's':
                    plt.title('Result after ' + str(round(timesteps[j])) + '/' + str(round(timesteps[-1])) + ' seconds')
                elif plot_unit == 'min':
                    plt.title('Result after ' + str(round(timesteps[j]/60)) + '/' + str(round(timesteps[-1]/60)) + ' minutes')
                elif plot_unit == 'hrs':
                    plt.title('Result after ' + str(round(timesteps[j]/3600)) + '/' + str(round(timesteps[-1]/3600)) + ' hours')
                else:
                    plt.title('Result after ' + str(round(timesteps[j]/(3600*24))) + '/' + str(round(timesteps[-1]/(3600*24))) + ' days')
    
            plt.savefig('Output/Results_3D_final_iteration_pressure.png', bbox_inches='tight')
            plt.close()

    # %% Return results of simulation to main script
        print('Plotting results finished at ' + str(datetime.datetime.now()), flush = True)
        t1 = time.time()
        print('Time elapsed for Plotting: ' + str(timedelta(seconds=(t1-t0))), flush = True)
    return N_abs,N_abs_iter,N_des,s,COM,area,active_facets,saturated,saturation_point,sticking_after,P
