# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 10:14:26 2022

@author: Peter Lindquist Henriksen
contact email 1: peter.lindquist.henriksen@cern.ch
contact email 2: peter@lindquist-henriksen.dk

"""
# Contains all functions to be called by other scripts of VacuumCOST.
import numpy as np
import matplotlib.pyplot as plt
from os.path import exists
import pickle
import copy
import warnings
from scipy.optimize import curve_fit
import zipfile
import xml.etree.ElementTree as ET



def get_geometries(data,N_facets,area,COM):
    # Returns facet area and COM coordinates based on facet vertices.
    # Calculation from  https://math.stackexchange.com/questions/3207981/how-do-you-calculate-the-area-of-a-2d-polygon-in-3d
    for i in range(N_facets): # Loop through all facets
        Num_v = len(data.find('Geometry/Facets')[i].find('Indices')[:])
        vertices = np.zeros([Num_v,4])
        for k in range(len(data.find('Geometry/Facets')[i].find('Indices')[:])): # Loop through all verticies for given facet
            vertices[k,0] = int(data.find('Geometry/Facets')[i].find('Indices')[k].attrib['vertex']) # Vertex ID
            vertices[k,1] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['x'])
            vertices[k,2] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['y'])
            vertices[k,3] = float(data.find('Geometry/Vertices')[int(vertices[k,0])].attrib['z'])
        runsum = 0
        for k in range(Num_v-1):
            runsum  = runsum + 0.5*np.cross((vertices[k,1:4]-vertices[0,1:4]),(vertices[k+1,1:4]-vertices[0,1:4]))
        area[i] = np.linalg.norm(runsum)
        # Get geometric center of mass
        COM[i,:] = sum(vertices[:,1:4])/Num_v 
    return area,COM



def get_facets_from_selection_group(data,selName):
    # Gets IDs of all facets in a named selection group
    selectionGroupFacets=[]
    groupNode=data.find('Interface/Selections/Selection[@name="'+selName+'"]') #Find first selection with name "selName"
    if groupNode != None: # if selection group name exists
        for selItemNode in groupNode.findall('selItem'):
            selectionGroupFacets.append(selItemNode.attrib['facet'])
    return np.asarray(selectionGroupFacets).astype(int)


def power_law(data_x, a, k):
    # Function for fitting curves to desorption yield data
    # NOTE: Warning in scipy for covariance is suppressed!
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        return a*np.power(data_x,k)


    
def load_and_fit_to_yield_data(use_previously_processed_texture_data,N_facets,texture_cell_flux,texture_cell_area,desorption_yield_data_files,power_law):
    # Load PSD yield data and fit functions to be used for interpolation/extrapolation to calculate outgassing rate
    if use_previously_processed_texture_data:
        if exists('Texture_files/cell_area_processed') and exists('Texture_files/SR_flux_processed') and exists('Texture_files/N_text_facets') and exists('Texture_files/has_texture') and exists('Texture_files/texture_facet_real_ID'):
            print('Loading previously processed texture data', flush=True)
            with open("Texture_files/cell_area_processed", "rb") as fp:
                cell_area = pickle.load(fp)
            with open("Texture_files/SR_flux_processed", "rb") as fp:
                SR_flux = pickle.load(fp)
            with open("Texture_files/N_text_facets", "rb") as fp:
                N_text_facets = pickle.load(fp)
            with open("Texture_files/has_texture", "rb") as fp:
                has_texture = pickle.load(fp)
            with open("Texture_files/texture_facet_real_ID", "rb") as fp:
                texture_facet_real_ID = pickle.load(fp)
        else:
            print('No previously processed texture data found', flush=True)
            use_previously_processed_texture_data = False
    if use_previously_processed_texture_data == False:
        print('Loading texture cell data and calculating SR flux densities...', flush=True)
        
        # Save array of strings with facet numbering to search in SR flux density file
        str_array = ["" for x in range(N_facets)]
        for i in range(N_facets):
            str_array[i] = 'FACET'+str(i+1)+'\n' # Facet numbering is here as in the GUI, 1-indexed
        
        # Read texture cell flux and area exported from SynRad.
        with open(texture_cell_flux, encoding='utf8') as f:
            cell_flux_lines = f.readlines()
        f.close()
        with open(texture_cell_area, encoding='utf8') as f:
            cell_areas_lines = f.readlines()
        f.close()
        
        cell_flux_lines = np.array(cell_flux_lines)
        cell_areas_lines = np.array(cell_areas_lines)
    

        array_10pct = np.round(np.linspace(len(str_array)/10, len(str_array)-1, 10)).astype(int) # Just for printing progress in terminal
        
        # Allocate
        line_no = np.zeros([N_facets,1])
        class structtype():
            pass
        cell_area = [ structtype() for i in range(N_facets)]
        SR_flux = [ structtype() for i in range(N_facets)]
        counter = 0
        text_fact_array = []
        has_texture = np.zeros([N_facets,1]).flatten()
        texture_facet_real_ID = np.zeros([N_facets,1]).flatten()
        # Read texture information from files and store in arrays
        for i in range(N_facets):
            if str_array[i] in cell_flux_lines: # If the facet exists in the SR flux density file
                line_no[i] = int(np.where(str_array[i]==cell_flux_lines[:])[0][0]) # Find the line number of the facet
                k = int(line_no[i])+1 # Start from the first line after the header
                j = 0
                if cell_flux_lines[k]!='No mesh.\n': # If the facet has a mesh
                    while cell_flux_lines[k]!='\n':
                        if j == 0: # Save first row of data
                            cell_area[counter] = np.fromstring(cell_areas_lines[k],sep='\t')
                            SR_flux[counter] = np.fromstring(cell_flux_lines[k],sep='\t')
                        else: # Append each row to the prevoius
                            cell_area[counter] = np.vstack([cell_area[counter],np.fromstring(cell_areas_lines[k],sep='\t')])
                            SR_flux[counter] = np.vstack([SR_flux[counter],np.fromstring(cell_flux_lines[k],sep='\t')])
                        k = k+1
                        j = j+1
                    text_fact_array.append(cell_areas_lines[int(line_no[i])])
                    has_texture[i] = 1 # This facet had texture
                    texture_facet_real_ID[counter] = i # Lookup table to find the facet ID of textured facet no. N
                    counter = counter+1
            if i in array_10pct: # Print progress
                 print(str(10*(np.where(array_10pct==i)[0][0]+1)) + '% complete.', flush=True)
    
        texture_facet_real_ID = texture_facet_real_ID[0:counter] # Save only lines for the textured facets. This reduces memory usage.
    
        # Number of facets with texture
        N_text_facets = counter
        # Save only information for the textured facets. This reduces memory usage.
        cell_area = cell_area[0:N_text_facets]
        SR_flux = SR_flux[0:N_text_facets]
    
        
        # Save files to be loaded later in case the simulation is run again for this geometry.
        with open("Texture_files/cell_area_processed", "wb") as fp:
            pickle.dump(cell_area, fp)
        with open("Texture_files/SR_flux_processed", "wb") as fp:
            pickle.dump(SR_flux, fp)
        with open("Texture_files/N_text_facets", "wb") as fp:
            pickle.dump(N_text_facets, fp)
        with open("Texture_files/has_texture", "wb") as fp:
            pickle.dump(has_texture, fp)
        with open("Texture_files/texture_facet_real_ID", "wb") as fp:
            pickle.dump(texture_facet_real_ID, fp)
            
    # Allocate lists
    SR_flux_dens = copy.deepcopy(cell_area)
    dynamic_outgassing_map = copy.deepcopy(cell_area)
    SR_dose_dens = copy.deepcopy(cell_area)
    
    # Calculate flux density
    for i in range(N_text_facets):
        # Make the division to calculate flux denisty but set flux dens = 0 where SR_flux == 0
        SR_flux_dens[i] = np.divide(SR_flux[i],cell_area[i], out=np.zeros_like(SR_flux[i]),where=SR_flux[i]!=0)
        
    
    # Allocate arrays for fit parameters
    N_des_yield = len(desorption_yield_data_files)
    len_yield_data = []    
    # Load desorption yield data
    for i in range(N_des_yield):
        desorption_yield_data = np.loadtxt(desorption_yield_data_files[i])
        len_yield_data = np.append(len_yield_data,len(desorption_yield_data))
    param_a = np.zeros([int(max(len_yield_data))-1,N_des_yield])
    param_k = np.zeros([int(max(len_yield_data))-1,N_des_yield])
    data_x_array = np.zeros([int(max(len_yield_data)),N_des_yield])

    
    for j in range(N_des_yield):
        # Load desorption yield data
        desorption_yield_data = np.loadtxt(desorption_yield_data_files[j])
        # Select x and y data tot use for fit
        data_x = desorption_yield_data[:,0]
        data_y = desorption_yield_data[:,1]
        # Store x data in array
        data_x_array[0:len(data_x),j] = data_x
        
        # Fit to data to obtain fit parameters for each part of the yield curve
        for i in range(round(len_yield_data[j])-1):
            init_guess = [1e4, -0.5]
            # May try to improve ease of fitting by providing reasonable initial guesses. Here based on Groebner_CU_CO desorption yield data which seems to work ok.
            if i==0:
                init_guess = [1.2e-2, -0.08]
            elif i==1:
                init_guess = [8e3, -0.4]
            elif i==2:
                init_guess = [2e6, -0.5]
            elif i==3:
                init_guess = [2.6e13, -0.9]
            with warnings.catch_warnings():
                # NOTE: Warning in scipy for covariance is suppressed!
                warnings.simplefilter("ignore")
                # Fitting
                pars, cov = curve_fit(f=power_law, xdata=data_x[i:i+2], ydata=data_y[i:i+2], p0=init_guess, bounds=(-np.inf, np.inf), maxfev=5000)
            # Store fit parameters
            param_a[i,j] = pars[0]
            param_k[i,j] = pars[1]
        # Plot data and fitted curves to be checked by user later.
        fig1, ax1 = plt.subplots(figsize=(10, 6))
        ax1.scatter(data_x,data_y,label='Desorption yield data')
        colors = []
        for i in range(sum(data_x_array[:,j]>0)-1):
            p = ax1.plot(data_x[i:i+2],power_law(data_x[i:i+2], param_a[i,j],param_k[i,j]),label='Fit to part '+str(i+1))
            colors.append(p[0].get_color())
        # Add extrapolation lines
        ax1.plot([0.1*data_x[0],data_x[0]],power_law([0.1*data_x[0],data_x[0]], param_a[0,j],param_k[0,j]),'--',color=colors[0])
        ax1.plot([data_x[-1],data_x[-1]*10],power_law([data_x[-1],data_x[-1]*10], param_a[i,j],param_k[i,j]),'--',color=colors[-1]) # i is here last index valid for this desorption map. There may be zero apdding for indices > i
        
        ax1.set_xlabel('Dose (ph/cm$^2$)',fontsize=12)
        ax1.set_ylabel('Yield (molec/ph)',fontsize=12)
        plt.xlim([0.1*data_x[0],data_x[-1]*10])
        # plt.ylim(ylimits)
        ax1.set_xscale('log')
        ax1.set_yscale('log')
        ax1.legend(loc="upper right",fontsize=12)
        ax1.tick_params(labelsize=12)
        plt.tight_layout()
        plt.savefig('Output/Desorption_yield_data_and_fits_map'+str(j+1)+'.png', bbox_inches='tight')
        plt.close()
    print('Make sure you check file: Output/Desorption_yield_data_and_fits.png to ensure fits to the data look reasonable!', flush=True)
    return cell_area,data_x_array,N_text_facets,SR_flux_dens,SR_dose_dens,param_a,param_k,dynamic_outgassing_map,has_texture,SR_flux,texture_facet_real_ID
    


def write_desorption_and_calculate_outgassing_rate(data,N_facets,has_texture,dynamic_outgassing_map,SR_flux,timesteps,run_iter,initializing,T0,T,N_A,outgassing,runtype,Ndes_to_run,N_iter,Ndes_per_second_array,initial_cond_time):
    # Overwrite data with new outgassing map and calculate the total outgassing rate from this map
    counter = 0
    for i in range(N_facets):
        # Only update if this facet has textured outgassing and uses the outgassing file
        if has_texture[i]==1:
            if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['useOutgassingFile']):   
                # Loop through outgassing map calculated for this facet and concatenate to a string
                textstring = str('\n')
                height = int(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['height'])
                for j in range(height):
                    # Precision up to 1e-40
                    if height == 1:
                        rowstring = np.char.mod('%.40f', dynamic_outgassing_map[counter][:]) # Convert each row to strings
                    else:
                        rowstring = np.char.mod('%.40f', dynamic_outgassing_map[counter][j,:]) # Convert each row to strings
                    temp_str = "\t".join(rowstring)+str('\n') # Join with tab seperator and end line with line break
                    textstring = textstring+temp_str # Add each row of outgassing map to the string
                        
                # overwrite outgassing map in the data with this new string
                data.find('Geometry/Facets')[i].find('DynamicOutgassing/map').text = textstring
                # Save new totalOutgassing and totalDose
                if height == 1:
                    data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalOutgassing'] = str(sum(dynamic_outgassing_map[counter]))
                    if initializing:
                        data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalDose'] = str(sum(SR_flux[counter])*initial_cond_time[i])
                    else:
                        data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalDose'] = str(sum(SR_flux[counter])*(timesteps[run_iter]+initial_cond_time[i]))
                else:
                    data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalOutgassing'] = str(sum(sum(dynamic_outgassing_map[counter])))
                    if initializing:
                        data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalDose'] = str(sum(sum(SR_flux[counter]))*initial_cond_time[i])
                    else:
                        data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalDose'] = str(sum(sum(SR_flux[counter]))*(timesteps[run_iter]+initial_cond_time[i]))
                
            counter = counter+1 # Count through textured facets
    
        # Re-load outgassing based on new desorption
        if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['desType']) != 0: # If desorption type is not "None"
            if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['useOutgassingFile']) == 1: # If outgassing comes from a file (dynamic)
                outgassing_mbarls = 10*float(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalOutgassing']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
            else:
                outgassing_mbarls = 10*float(data.find('Geometry/Facets')[i].find('Outgassing').attrib['constValue']) # Multiply by 10 to convert from Pa*m3/s to mbar*l/s
        else:
            outgassing_mbarls = 0
        outgassing_mols = outgassing_mbarls*4.41e-5*(T0/T[i]) #convert to mol/s # https://www.pfeiffer-vacuum.com/en/know-how/introduction-to-vacuum-technology/fundamentals/pv-throughput/
        outgassing[i] = outgassing_mols*N_A # molecules/s
        
    Ndes_per_second = np.sum(outgassing) # total outgassing rate in molecules/s
    if runtype == 'By number of molecules':
        Ndes_per_second = Ndes_to_run # Doesnt matter the outgassing rate,  as a fixe number of molecules are simulated anyway.
        if initializing:
            print('Simulating ' + str(Ndes_to_run) + ' molecules in each of ' + str(N_iter-1) + ' iterations for a total of ' + str(Ndes_to_run*(N_iter-1)) + ' molecules', flush=True)
    if initializing:
        Ndes_per_second_array[0] = Ndes_per_second # To check later whether desorption changed too much
        print('Simulating system during a total of ' + str(timesteps[-1]) + ' seconds', flush=True)
    else:
        Ndes_per_second_array[run_iter+1] = Ndes_per_second # To check later whether desorption changed too much 
        
    return data,Ndes_per_second,Ndes_per_second_array


    
def calculate_dose_and_outgassing(N_text_facets,initializing,SR_flux_dens,SR_dose_dens,timesteps,run_iter,data_x_array,idx_t_array,idx_array,param_a,param_k,parameters_a_array,parameters_k_array,power_law,cell_yield_map,cell_area,N_A,T0,T,dynamic_outgassing_map,texture_facet_real_ID,selection_group_facet_IDs,initial_cond_time):
    # Calculates the dose and outgassing map on each texture cell
    for i in range(N_text_facets): # Go only through textured facets
        # Calculate synchrotron radiation dose density
        if initializing:
            SR_dose_dens[i] = SR_flux_dens[i]*initial_cond_time[int(texture_facet_real_ID[i])]
        else:
            SR_dose_dens[i] = SR_flux_dens[i]*(timesteps[run_iter]+initial_cond_time[int(texture_facet_real_ID[i])])
        k = 0 # Yield map index - which of the fitted desorption yield data the current facet belongs to
        facet_belongs_to_material_group = False
        for j in range(len(selection_group_facet_IDs)): # Loop through selection groups
            if any(selection_group_facet_IDs[j]==texture_facet_real_ID[i]): # If facet is this selection group
                k = j # The yield map index that this facet belongs to.
                facet_belongs_to_material_group = True
        if facet_belongs_to_material_group==False and initializing:
            print('Warning: Facet '+str(int(texture_facet_real_ID[i]+1))+' has texture but does not belong to a listed selection group. It was assigned the material of the first yield data file listed.')
        # Now calculate the yield from each texture cell
        idx_t = copy.deepcopy(idx_t_array[k])
        data_x = data_x_array[0:sum(data_x_array[:,k]>0),k] # Take only the x data for the yield map relevant for this facet
        for j in range(len(data_x)):
            idx_t[j] = data_x[j]<SR_dose_dens[i] # Find for each yield function curve, which cells have higher dose than applicable
        idx_array[i] = np.sum(idx_t,axis=0) # Sum over the first axis (yield curves) to find the yield curve index applicable for each cell
        idx_array[i][idx_array[i]==0] = 1 # Any cell with lower yield than first data point, belongs to first yield function
        idx_array[i][idx_array[i]==len(data_x)] = len(data_x)-1 # Any cell with higher yield than last data point, belongs to last yield function
        idx_array[i] = idx_array[i]-1 # Subtract 1 because Python is zero-indexed
        #Make array containing which function parameters to use for each texture cell (how far along the PSD data curve it is)
        for j in range(len(data_x)-1):
            parameters_a_array[i][idx_array[i] == j] = param_a[j,k]
            parameters_k_array[i][idx_array[i] == j] = param_k[j,k]
        # Calculate yield for each texture cell
        cell_yield_map[i] = power_law(SR_dose_dens[i], parameters_a_array[i],parameters_k_array[i]) # Find yield for this texture cell (molec/photon)
        cell_yield_map[i][SR_dose_dens[i]==0] = 0 # Yield for 0 dose density should be 0, not inf as the data extrapolation produces
        cell_yield_map[i][np.isnan(SR_flux_dens[i])] = 0 # Yield for nan dose density should be 0      
        #Convert from molecule/photon to molecule/s
        cell_yield_map_molec_s = cell_yield_map[i]*SR_flux_dens[i]*cell_area[i]
        #Convert to Pa*m3/s
        dynamic_outgassing_map[i] = cell_yield_map_molec_s/(10*N_A*4.41e-5*(T0/T[i]))
        # Convert nan values to 0
        dynamic_outgassing_map[i][np.isnan(dynamic_outgassing_map[i])] = 0
    return dynamic_outgassing_map




def calculate_sticking(sticking_model,N_facets,active_facets,N_abs,run_iter,area,new_s,s,Delta_s_rel,sticking_change_calculation,data,AC,N_A,CO_dens,initializing,calc_stick_after):
    # Calculates the new sticking coefficients for facets depending on the sticking model and the number of absorbed molecules

    # Dummy variables so there is something to return no matter the sticking model
    x0 = 1
    start_stick = 1
    
    # Update sticking
    if sticking_model=='CO_physics_model':
        x0 = 2e15 # Point of NEG saturation (molec/cm2)
        start_stick = 0.7
        # Overwrite sticking parameter in tree
        for i in range(N_facets):
            if active_facets[i]==1:
                if initializing:
                    new_s[i] = start_stick
                else:
                    if float(N_abs[i,run_iter]/area[i])<x0: # cross-over for given number of absorbed molecules per area
                        new_s[i] = start_stick*(1-((N_abs[i,run_iter]/area[i])/x0)) # new sticking
                    else:
                        new_s[i] = 0 # sticking 0 if saturated
                    # Calcuate relative change from previous iteration
                    if s[i,run_iter]==0: # If prior sticking 0, disregard facet
                        Delta_s_rel[i] = 0
                    else:
                        if sticking_change_calculation=='Relative to previous':
                            Delta_s_rel[i] = (s[i,run_iter]-new_s[i])/s[i,run_iter]
                        else:
                            Delta_s_rel[i] = (s[i,run_iter]-new_s[i])/s[i,0]
            else:
                new_s[i] = s[i,run_iter] # If facet not active, keep the sticking constant.
            if calc_stick_after==False: # Only write to data if in simulation (calculating sticking before each iteration) as apposed to during processing of a previous simulation
                data.find('Geometry/Facets')[i].find('Sticking').attrib['constValue'] = ' '.join(map(str, new_s[i]))
    elif sticking_model=='H2_CO_dependent':
        x0 = 2e15 # Point of NEG saturation (CO molec/cm2)
        start_stick = 8e-3
        # Overwrite sticking parameter in tree
        for i in range(N_facets):
            if active_facets[i]==1:
                if initializing:
                    new_s[i] = start_stick
                else:
                    if CO_dens[i,run_iter]<x0: # cross-over for given number of absorbed molecules per area
                        new_s[i] = start_stick*pow(1-(CO_dens[i,run_iter]/x0),2) # new sticking
                    else:
                        new_s[i] = 0 # sticking 0 if saturated
                    # Calcuate relative change from previous iteration
                    if s[i,run_iter]==0: # If prior sticking 0, disregard facet
                        Delta_s_rel[i] = 0
                    else:
                        if sticking_change_calculation=='Relative to previous':
                            Delta_s_rel[i] = (s[i,run_iter]-new_s[i])/s[i,run_iter]
                        else:
                            Delta_s_rel[i] = (s[i,run_iter]-new_s[i])/s[i,0]
            else:
                new_s[i] = s[i,run_iter] # If facet not active, keep the sticking constant.
            if calc_stick_after==False: # Only write to data if in simulation (calculating sticking before each iteration) as apposed to during processing of a previous simulation
                data.find('Geometry/Facets')[i].find('Sticking').attrib['constValue'] = ' '.join(map(str, new_s[i]))
    else:
        print('No valid sticking model chosen', flush = True)
        
    return new_s,Delta_s_rel,data,x0,start_stick


def store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,j,which_to_store):
    # Reads the results of a simulation and stores the number of absorbed and desorbed particles from each facet in each iteration.
    # The variable which_to_store determines whether to store virtual molecules absorbed or desorbed (1 or 2), or calculate the corresponding number of physical molecules (3 and 4)
    j = run_iter
    if which_to_store==1: # Store the number of virtual absorbed molecules
        for i in range(len(data.find('MolflowResults/Moments/Moment/FacetResults')[:])):
            N_abs_before_scaling[i,j] = int(data.find('MolflowResults/Moments/Moment/FacetResults')[i].find('Hits').attrib['nbAbsEquiv'])
        return N_abs_before_scaling

    elif which_to_store==2: # Store the number of virtual desorbed molecules
        for i in range(len(data.find('MolflowResults/Moments/Moment/FacetResults')[:])):
            N_des_before_scaling[i,j] = int(data.find('MolflowResults/Moments/Moment/FacetResults')[i].find('Hits').attrib['nbDes'])
        return N_des_before_scaling
    
    elif which_to_store==3: # Store the number of physical absorbed molecules. The scaling is No_phys/No_virt normalized to the time.
        for i in range(len(data.find('MolflowResults/Moments/Moment/FacetResults')[:])):
            if runtype == 'By timesteps':
                if j == 0:
                    N_abs[i,j] = scaling[j]*timesteps[j]*N_abs_before_scaling[i,j]
                else:
                    N_abs[i,j] = scaling[j]*(timesteps[j]-timesteps[j-1])*N_abs_before_scaling[i,j] + N_abs[i,j-1]
            if runtype == 'By automatic temporal resolution':
                if j == 0:
                    N_abs[i,j] = scaling[j]*timesteps[j]*N_abs_before_scaling[i,j]
                else:
                    N_abs[i,j] = scaling[j]*(timesteps[j]-timesteps[j-1])*N_abs_before_scaling[i,j] + N_abs[i,j-1]
            elif runtype == 'By number of molecules':
                if j == 0:
                    N_abs[i,j] = scaling[j]*N_abs_before_scaling[i,j]
                else:
                    N_abs[i,j] = scaling[j]*N_abs_before_scaling[i,j] + N_abs[i,j-1]
        return N_abs

    elif which_to_store==4: # Store the number of physical desorbed molecules. The scaling is No_phys/No_virt normalized to the time.
        for i in range(len(data.find('MolflowResults/Moments/Moment/FacetResults')[:])):
            if runtype == 'By timesteps':
                if j == 0:
                    N_des[i,j] = scaling[j]*timesteps[j]*N_des_before_scaling[i,j]
                else:
                    N_des[i,j] = scaling[j]*(timesteps[j]-timesteps[j-1])*N_des_before_scaling[i,j] + N_des[i,j-1]
            if runtype == 'By automatic temporal resolution':
                if j == 0:
                    N_des[i,j] = scaling[j]*timesteps[j]*N_des_before_scaling[i,j]
                else:
                    N_des[i,j] = scaling[j]*(timesteps[j]-timesteps[j-1])*N_des_before_scaling[i,j] + N_des[i,j-1]
            elif runtype == 'By number of molecules':
                if j == 0:
                    N_des[i,j] = scaling[j]*N_des_before_scaling[i,j]
                else:
                    N_des[i,j] = scaling[j]*N_des_before_scaling[i,j] + N_des[i,j-1]
        return N_des


def add_timestep(run_iter,timesteps,Ndes_per_second_array,N_abs,z,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,timesteps_tested,add_reason,sticking_changed,leak_time):
    # Add intermediate timestep and return new timesteps vector
    if run_iter==0:
        new_timestep = timesteps[run_iter]/2 # If first iteration, cut time in half
    else:
        new_timestep = np.mean([timesteps[run_iter],timesteps[run_iter-1]]) # Otherwise take the mean of current and previous time step
    if timesteps[run_iter]-new_timestep<1:
        print('New time step only ' + str(timesteps[run_iter]-new_timestep) + ' s more than current. Decrease criteria to bring required temporal resolution below 1/s.', flush=True)
        return
    new_timestep = np.floor(new_timestep) # Round down to the second
    if add_reason=='leak':
        new_timestep = np.ceil(leak_time)+1 # Make sure temporal resolution is not higher than 1/s
        print('Leak started at ' + str(round(leak_time)) + ' s. Intermediate step inserted at ' + str(int(new_timestep)) + ' s.', flush = True)
    timesteps = np.insert(timesteps, run_iter, new_timestep) # Insert new time step in timesteps vector
    if add_reason=='sticking':
        print('Mean change of sticking was ' + str(round(100*np.mean(sticking_changed),2)) + '%. Intermediate step inserted at ' + str(int(new_timestep)) + ' s.', flush = True)
    elif add_reason=='desorption':
        print('Relative change of desorption was ' + str(round(100*(Ndes_per_second_array[run_iter]-Ndes_per_second_array[run_iter+1])/Ndes_per_second_array[run_iter],2)) + '%. Intermediate step inserted at ' + str(int(new_timestep)) + ' s.', flush = True)
    
    # Expand pre-allocated arrays by 1
    N_abs = np.concatenate((N_abs ,z), axis=1)
    N_abs_before_scaling = np.concatenate((N_abs_before_scaling ,z), axis=1)
    N_des = np.concatenate((N_des ,z), axis=1)
    N_des_before_scaling = np.concatenate((N_des_before_scaling ,z), axis=1)
    s = np.concatenate((s ,z), axis=1)
    AC = np.concatenate((AC ,z), axis=1)
    scaling = np.concatenate((scaling ,np.zeros([1,1]).flatten()), axis=0)
    Ndes_per_second_array = np.concatenate((Ndes_per_second_array ,np.zeros([1,1]).flatten()), axis=0)
    # Update number of iterations
    N_iter = len(timesteps)
    timesteps_tested = np.append(timesteps_tested,new_timestep)
    return timesteps,N_abs,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,Ndes_per_second_array,N_iter,timesteps_tested


def parse_molflow_file(filename):
    # Function contributed by Matthew Cox
    # Read and parse MolFow or SynRad file into an element tree
    # Returns the root node of the tree
    # Compressed or uncompressed file
    # Assumed xml format
    
    # Unzip it first if we need to
    # Check for zipped file extension
    if 'zip' in filename or 'syn7z' in filename:
        # open the zip archive read only
        with zipfile.ZipFile(filename,'r') as archive:
            # Find the first file in the archive with the correct extension which we need
            filenames = archive.namelist()
            for fn in filenames:
                if 'syn' in fn or 'xml' in fn:
                    # Read the xml content and parse into an element tree
                    xml_content = archive.read(fn)
                    xml_root = ET.fromstring(xml_content)
                    # And then break out of the for loop and stop looking for more files
                    break
    else:
        # File was not compressed so just read and parse into tree root directly
        xml_root = ET.parse(filename).getroot()
        
    # Return the element root node
    return xml_root
                    

def write_molflow_zip_file(data,file_to_save,filename):
    # Writes data in element tree to .xml inside .zip file.
    with zipfile.ZipFile(file_to_save, 'w', zipfile.ZIP_DEFLATED) as z:
            with z.open(filename, 'w') as f:
                tree = ET.ElementTree(data)
                tree.write(f, encoding="utf-8", xml_declaration=True)
    return