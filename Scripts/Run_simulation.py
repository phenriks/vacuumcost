# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 08:36:05 2022

@author: Peter Lindquist Henriksen
contact email 1: peter.lindquist.henriksen@cern.ch
contact email 2: peter@lindquist-henriksen.dk

"""

import numpy as np
# import xml.etree.ElementTree as ET
import subprocess
import datetime
from worker import store_abs_des,calculate_sticking,load_and_fit_to_yield_data,power_law,calculate_dose_and_outgassing,write_desorption_and_calculate_outgassing_rate,add_timestep,get_geometries,get_facets_from_selection_group,parse_molflow_file,write_molflow_zip_file
import time
from datetime import timedelta
import copy
from os.path import exists
from shutil import copyfile

def Run_simulation(path_to_molflowCLI,save_iterations_folder,file_to_run_name,file_to_run_type,N_iter,Ndes_to_run,sticking_model,runtype,runtime,timesteps,max_Delta_s,lower_lim_s_timeupdate,facet_inactive_critera_sticking,facet_inactive_critera_ID,facet_inactive_selection_group_names,iteration_runtime,cpu_time_per_iter,scaling_factor,sticking_change_calculation,desorption_yield_data_files,Material_selection_group_names,texture_cell_flux,texture_cell_area,desorption_type,max_Delta_desorption,use_previously_processed_texture_data,leak_facet):
    # Script to run the actual VacuumCOST simulation
    print('Simulation started at ' + str(datetime.datetime.now()), flush = True)
    print('Sticking model: ' + sticking_model, flush = True)
    print('Runtype: ' + runtype, flush = True)
    print('Desorption: ' + desorption_type, flush = True)
    
    t0 = time.time()
    # %% Set up simulation
    reset = True # Reset means current simulation status (e.g. number of impacts, absoroptions, etc.) is reset before run
    # Here True because we set up new conditions every time (new sticking) and anyway add previous absorbed molecules
    
    CO_dens = 1  #Dummy variable, not used if sticking model does not need it.
    if sticking_model=='H2_CO_dependent': # Load data from CO simulation
        if exists('Output/CO_dens.csv'):
            CO_dens = np.loadtxt('Output/CO_dens.csv', delimiter=',')
            if exists('Output/timesteps_CO_physics_model.csv'):
                timesteps = np.loadtxt('Output/timesteps_CO_physics_model.csv', delimiter=',')
            else:
                print('Error: You must run a CO simulation and put timesteps_CO_physics_model.csv in output folder before simulating H2!', flush=True)
                return
            runtype = 'By timesteps'
        else:
            print('Error: You must run a CO simulation and put CO_dens.csv in output folder before simulating H2!', flush=True)
            return
        
    # %% Copy runfile to iterations folder and rename.
    file_to_run = file_to_run_name + file_to_run_type
    file_to_save = save_iterations_folder + file_to_run_name + "_iter"+str(0) + file_to_run_type

    if exists(file_to_run):
        copyfile(file_to_run, file_to_save)
    else:
        print("File to run does not exist in current directory.")
        return

    
    # %% Allocate arrays etc.
    
    # Start by reading dummy data
    data = parse_molflow_file(file_to_save)
    
    # Check and print warning if user changed sticking model but forgot to change gas mass in model or desorption yield data file.
    if sticking_model=='H2_CO_dependent':
        if int(data.find('MolflowSimuSettings/Gas').attrib['mass'])!=2:
            print('Warning: Gas molecular mass in Molflow file is not 2.')
        if not any('H2' in s for s in desorption_yield_data_files) and desorption_type == 'Dynamic PSD from Synrad data':
            print('Warning: Ensure correct desorption yield data is used.')
    
    # Convert runtypes to timesteps (except for when running # of molecules)
    if runtype == 'By timesteps':
        timesteps = np.append(timesteps,timesteps[-1])
        N_iter = len(timesteps)
    elif runtype == 'By automatic temporal resolution':
        timesteps = np.append(timesteps,timesteps[-1]) # Add again last timestep to allow code to check for Delta s too large
        N_iter = len(timesteps)
    elif runtype == 'By physical time elapsed exponential': # Convert anyway to timesteps
        timesteps = np.power(runtime,np.arange(N_iter)) # Step spacing by power law
        timesteps = np.append(timesteps,timesteps[-1])
        N_iter = len(timesteps)
        runtype = 'By timesteps'
    elif runtype == 'By physical time elapsed linear': # Convert anyway to timesteps
        timesteps = runtime*(np.arange(N_iter)+1) # Linear step spacing
        timesteps = np.append(timesteps,timesteps[-1])
        N_iter = len(timesteps)
        runtype = 'By timesteps'
    elif runtype == 'By number of molecules': # Convert anyway to timesteps
        N_iter = N_iter+1
    
    # Allocate arrays to be used later
    timesteps_tested = []
    N_facets = len(data.find('MolflowResults/Moments/Moment/FacetResults')[:])
    N_abs = np.zeros([N_facets,N_iter-1])
    N_abs_before_scaling = np.zeros([N_facets,N_iter-1])
    N_des = np.zeros([N_facets,N_iter-1])
    N_des_before_scaling = np.zeros([N_facets,N_iter-1])
    s = np.zeros([N_facets,N_iter-1])
    new_s = np.zeros([N_facets,1])
    Delta_s_rel = np.zeros([N_facets,1])
    AC = np.zeros([N_facets,N_iter-1])
    active_facets = np.ones([N_facets,1]).flatten() #Start as ones for logical selection later
    initial_cond_time = np.zeros([N_facets,1]).flatten() #Start as zero conditioning time for all facets before possibly overwriting when loading file to run
    area = np.zeros([N_facets,1])
    COM = np.zeros([N_facets,3])
    outgassing = np.zeros([N_facets,1])
    scaling = np.zeros([1,N_iter-1]).flatten()
    Ndes_per_second_array = np.zeros([1,N_iter]).flatten() # Offset by 1 so Ndes_per_second_array[n] is the desorption by second after time timesteps[n-1]
    T = np.zeros([N_facets,1]).flatten()
    
    # Array to be used for concatenation in order to expand pre-allocated arrays in case of added timesteps
    z = np.zeros((N_facets, 1), dtype=N_abs.dtype)

    # Constants
    T0 = 273.15 # 0 deg C in K
    N_A = 6.02214076e23 # Avogadro's constant (molecules/mol)
    
    
    calc_stick_after = False # Calculate sticking before each new simulation
    leak_warning = True # Warn in case of leaks


    # %% Load results from dummy simulation and define correcting starting conditions (NEG sticking, desorption)
    
    for i in range(N_facets): # Loop through all facets
        # Load temperature of each facet
        T[i] = float(data.find('Geometry/Facets')[i].find('Temperature').attrib['value'])
        # Load sticking coefficient
        s[i,0] = data.find('Geometry/Facets')[i].find('Sticking').attrib['constValue']
        
        if desorption_type=='Dynamic PSD from Synrad data' or desorption_type == 'Constant outgassing with texture': # Determine whether facet is pre-conditioned
            if int(data.find('Geometry/Facets')[i].find('Outgassing').attrib['useOutgassingFile']): # Only use dynamic conditioning if facet has and uses dynamic outgassing map
                totalFlux = float(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalFlux'])
                totalDose = float(data.find('Geometry/Facets')[i].find('DynamicOutgassing').attrib['totalDose'])
                if totalDose>0: # Only use if facet exposed to SR and has nonzero dose
                    initial_cond_time[i] = totalDose/totalFlux # Conditioning time before starting this simulation
            
        
    # Determination of facets to be considered active (Boolean array). "Active" means having sticking coefficient updated in each iteration.
    for i in range(len(facet_inactive_critera_sticking)): # Defined by initial sticking factor
        active_facets = active_facets*(np.around(s[:,0],decimals=4)!=facet_inactive_critera_sticking[i])
    for i in range(len(facet_inactive_critera_ID)): # Defined by facet ID
        active_facets[facet_inactive_critera_ID[i]-1] = 0 # -1 to translate from Molflow numbering (first facet has ID 1) to data file numbering (first facet has ID 0)
    for i in range(len(facet_inactive_selection_group_names)): # Loop through each named selection group
        active_facets[get_facets_from_selection_group(data,facet_inactive_selection_group_names[i])] = 0 # Make facets in named selection group inactive
    active_facets = active_facets==1 # Make boolean

    
    
    # Get area and COM coordinates of facets
    area,COM = get_geometries(data,N_facets,area,COM)


    # Set up initial parameters
    initializing = True
    has_texture = np.zeros([N_facets,1]).flatten()
    dynamic_outgassing_map = []
    SR_flux = []
    run_iter = 0
    # Make new sticking calculation
    new_s,Delta_s_rel,data,x0,start_stick = calculate_sticking(sticking_model,N_facets,active_facets,N_abs,run_iter,area,new_s,s,Delta_s_rel,sticking_change_calculation,data,AC,N_A,CO_dens,initializing,calc_stick_after)


    # Set up leak if present
    leak_started = False
    leak_time = 0 # Dummy variable, will be overwritten later
    if leak_facet.size>0: # If a leaky facet is defined
        N_leaks = len(leak_facet[:,0])
        leak_rate = np.zeros([N_leaks,1]).flatten()
        leak_started = np.full((N_leaks, 1), False).flatten() # Initially all leaks have not started
        for i in range(N_leaks):
            leak_rate[i] = float(data.find('Geometry/Facets')[int(leak_facet[i,0]-1)].find('Outgassing').attrib['constValue']) # Save leak rate for later
            data.find('Geometry/Facets')[int(leak_facet[i,0]-1)].find('Outgassing').attrib['constValue'] = "0" # Set leak rate to 0 to start with
        np.savetxt('Output/leak_rate.csv', leak_rate, delimiter=',') # Save leak rates in case it needs to be loaded by script to continue simulation later.


    # If update desorption based on Synrad files and PSD yield, calculate yield curves, cell fluxes, etc.
    if desorption_type == 'Dynamic PSD from Synrad data':
        # Load PSD yield data and fit curves to be later used for interpolation/extrapolation depending on the dose on each texture cell.
        cell_area,data_x_array,N_text_facets,SR_flux_dens,SR_dose_dens,param_a,param_k,dynamic_outgassing_map,has_texture,SR_flux,texture_facet_real_ID = load_and_fit_to_yield_data(use_previously_processed_texture_data,N_facets,texture_cell_flux,texture_cell_area,desorption_yield_data_files,power_law)
        

        # Allocate arrays used for calculating the desoprtion from texture cells
        cell_yield_map = copy.deepcopy(cell_area)
        idx_array = copy.deepcopy(cell_area)
        parameters_a_array = copy.deepcopy(cell_area)
        parameters_k_array = copy.deepcopy(cell_area)
        idx_t = copy.deepcopy(cell_area) # used for finding the index of the yield data x-axis corresponding to the current position on the PSD curve
        idx_t_array = [idx_t for rows in range(len(desorption_yield_data_files))] # Make an array for each PSD data file used

        # Find facet IDs for each selection group
        selection_group_facet_IDs = [None]*len(Material_selection_group_names) # Allocate
        for i in range(len(Material_selection_group_names)): # Loop through each selection group
            idx_t_array[i] = idx_t_array[i][0:sum(data_x_array[:,i]>0)] # Find index where PSD x-data exists. Since different yield maps have different data lengths, zero-padding is used to regularize array.
            selection_group_facet_IDs[i] = get_facets_from_selection_group(data,Material_selection_group_names[i]) # Get facet IDs
        
        # Calculate the outgassing for each texture cell
        dynamic_outgassing_map = calculate_dose_and_outgassing(N_text_facets,initializing,SR_flux_dens,SR_dose_dens,timesteps,run_iter,data_x_array,idx_t_array,idx_array,param_a,param_k,parameters_a_array,parameters_k_array,power_law,cell_yield_map,cell_area,N_A,T0,T,dynamic_outgassing_map,texture_facet_real_ID,selection_group_facet_IDs,initial_cond_time)

    elif desorption_type == 'Constant outgassing with texture':
        # Load PSD yield data and fit curves to be later used for interpolation/extrapolation depending on the dose on each texture cell.
        cell_area,data_x_array,N_text_facets,SR_flux_dens,SR_dose_dens,param_a,param_k,dynamic_outgassing_map,has_texture,SR_flux,texture_facet_real_ID = load_and_fit_to_yield_data(use_previously_processed_texture_data,N_facets,texture_cell_flux,texture_cell_area,desorption_yield_data_files,power_law)

    # Write to results to data and find the total outgassing rate (molecules/s)
    data,Ndes_per_second,Ndes_per_second_array = write_desorption_and_calculate_outgassing_rate(data,N_facets,has_texture,dynamic_outgassing_map,SR_flux,timesteps,run_iter,initializing,T0,T,N_A,outgassing,runtype,Ndes_to_run,N_iter,Ndes_per_second_array,initial_cond_time)

    initializing = False # No longer initializing simulation beyond this point
    
    
    
    # %% Write the new parameters to a new xml file
    filename = file_to_run_name + "_iter"+str(0)+".xml"
    write_molflow_zip_file(data,file_to_save,filename)
    
    # %% Run the first iteration of simulation
    
    print('Starting Molflow simulation iteration ' + str(0) + '/' + str(N_iter-1) + ' at ' + str(datetime.datetime.now()), flush = True)
    
    if iteration_runtime == 'By scaling factor':
        subprocess.run([path_to_molflowCLI, "--file", file_to_save, "-o", file_to_save, "-d", str(int(Ndes_per_second/scaling_factor)), "--reset"], 
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT)
    elif iteration_runtime == 'By CPU time':
        subprocess.run([path_to_molflowCLI, "--file", file_to_save, "-o", file_to_save, "-t", str(cpu_time_per_iter), "--reset"], 
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT)
    print('Iteration ' + str(0) + ' complete at ' + str(datetime.datetime.now()), flush = True)
    
    timesteps_tested = np.append(timesteps_tested,timesteps[0]) # Keep track of all time steps tested in the simulation
    

        
    # %% Start run
    tt = 0 # Keeping track of timesteps_tested
    run_iter = 0 # Keeping track of iterations simulated in Molflow
    cont = True # Start by continuing with the next iteration
    while run_iter < N_iter-1: # As long as this iteration is not the last iteration.
        # Files to read and save in this iteration
        if cont: # Only parse data (again) if continue to next iteration
            file_to_run = save_iterations_folder + file_to_run_name + "_iter"+str(run_iter) + file_to_run_type       
            file_to_save = save_iterations_folder + file_to_run_name + "_iter"+str(run_iter+1) + file_to_run_type
        
            # Parse results from this iteration
            data = parse_molflow_file(file_to_run)
        
            # Load sticking coefficient used for iteration
            for i in range(N_facets):
                s[i,run_iter] = data.find('Geometry/Facets')[i].find('Sticking').attrib['constValue']


        # %% Apply scaling to convert from virtual to real molecules
        # Find the number of absorbed and desorbed (virtual) molecules from each facet before scaling
        N_abs_before_scaling = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,tt,1)
        N_des_before_scaling = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,tt,2)

        # Calculate scaling factor to use
        if iteration_runtime == 'By scaling factor':
            scaling[run_iter] = Ndes_to_run/int(Ndes_to_run/scaling_factor) # Ndes_to_run is only used for regularizing the scaling so MolflowCLI can run it
        elif iteration_runtime == 'By CPU time':
            scaling[run_iter] = Ndes_per_second/sum(N_des_before_scaling[:,run_iter]) # Ratio of real to virtual molecules normalized to the second.
        
        # Store the number of absorbed and desorbed (physical) molecules from each facet in this iteration after appropraite scaling
        N_abs = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,tt,3)
        N_des = store_abs_des(data,runtype,runtime,timesteps,scaling,run_iter,N_abs_before_scaling,N_abs,N_des_before_scaling,N_des,tt,4)
        
        # Print a warning in case the model has leaks. Implemented in two different ways to be sure...
        diff_des_abs_rel = np.diff([sum(N_abs[:,run_iter]),sum(N_des[:,run_iter])])/sum(N_des[:,run_iter])
        numleaks = int(data.find('MolflowResults/Moments/Moment/Global/Hits').attrib['totalLeak'])
        if diff_des_abs_rel>0.01:
            print('Warning: It looks like your model has leaks. The difference between # of desorbed and absorbed molecules is more than 1%.', flush = True)
        if leak_warning:
            if numleaks>0:
                print('Warning: ' + '{:.2E}'.format(numleaks) + '/' + '{:.2E}'.format(sum(N_des_before_scaling[:,run_iter])) + ' = ' + str(round(100*numleaks/sum(N_des_before_scaling[:,run_iter]),2))+'% of desorbed molecules leaked out of the geometry. This warning will be suppresed for subsequent iterations.', flush = True)
                leak_warning = False
    
        # %% Calculate new sticking for each facet based on the number of absorbed molecules
        new_s,Delta_s_rel,data,x0,start_stick = calculate_sticking(sticking_model,N_facets,active_facets,N_abs,run_iter,area,new_s,s,Delta_s_rel,sticking_change_calculation,data,AC,N_A,CO_dens,initializing,calc_stick_after)
            
            
        # %% Test whether sticking changed too much in this iteration (if so, insert extra timestep)
        if runtype == 'By automatic temporal resolution':
            # Consider only facets that have had their sticking changed (relative change in sticking is nonzero)
            Delta_s_rel = Delta_s_rel.flatten()
            idx = Delta_s_rel != 0
            
            if sticking_change_calculation=='Relative to previous':
                idx = idx*(s[:,run_iter]>(s[:,0]*lower_lim_s_timeupdate))  # Consider facets that have sticking above threshold of initial sticking. Otherwise it will run forever as steps get ever smaller
            sticking_changed = Delta_s_rel[idx] # Relative change in sticking for facets under consideration

            if np.mean(sticking_changed)>max_Delta_s: # If change in sticking is above threhold
                # Add intermediate timestep
                add_reason = 'sticking'
                timesteps,N_abs,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,Ndes_per_second_array,N_iter,timesteps_tested = add_timestep(run_iter,timesteps,Ndes_per_second_array,N_abs,z,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,timesteps_tested,add_reason,sticking_changed,leak_time)
                cont = False # Do not continue with simulation at current step
            else:
                cont = True # Continue with simulation at current step
            
    
         # %% Make new desorption calculation 
        if cont:
            if desorption_type == 'Dynamic PSD from Synrad data':   
                # Calculate the outgassing for each texture cell
                dynamic_outgassing_map = calculate_dose_and_outgassing(N_text_facets,initializing,SR_flux_dens,SR_dose_dens,timesteps,run_iter,data_x_array,idx_t_array,idx_array,param_a,param_k,parameters_a_array,parameters_k_array,power_law,cell_yield_map,cell_area,N_A,T0,T,dynamic_outgassing_map,texture_facet_real_ID,selection_group_facet_IDs,initial_cond_time)
                # Write to data and find outgassing rate
                data,Ndes_per_second,Ndes_per_second_array = write_desorption_and_calculate_outgassing_rate(data,N_facets,has_texture,dynamic_outgassing_map,SR_flux,timesteps,run_iter,initializing,T0,T,N_A,outgassing,runtype,Ndes_to_run,N_iter,Ndes_per_second_array,initial_cond_time)
                
                # Test whether desorption changed too much (if so, insert extra timestep)
                if runtype == 'By automatic temporal resolution':
                    if (Ndes_per_second_array[run_iter]-Ndes_per_second_array[run_iter+1])/Ndes_per_second_array[run_iter] > max_Delta_desorption:
                        # Add intermediate timestep
                        add_reason = 'desorption'
                        timesteps,N_abs,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,Ndes_per_second_array,N_iter,timesteps_tested = add_timestep(run_iter,timesteps,Ndes_per_second_array,N_abs,z,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,timesteps_tested,add_reason,sticking_changed,leak_time)
                        cont = False # Do not continue with simulation at current step
                    else:
                        cont = True # Continue with simulation at current step

                        
        # %% Add leak if applicable
        if cont:
            if leak_facet.size>0: # If a leaky facet is defined
                for i in range(N_leaks):
                    if timesteps[run_iter]>=leak_facet[i,1]: # If current time step is at or after leak should occur
                        data.find('Geometry/Facets')[int(leak_facet[i,0]-1)].find('Outgassing').attrib['constValue'] = str(leak_rate[i]) # Set leak rate
                        if leak_started[i] == False:
                            # Add intermediate timestep for the leak start time
                            leak_time = leak_facet[i,1]
                            add_reason = 'leak'
                            timesteps,N_abs,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,Ndes_per_second_array,N_iter,timesteps_tested = add_timestep(run_iter,timesteps,Ndes_per_second_array,N_abs,z,N_abs_before_scaling,N_des,N_des_before_scaling,s,AC,scaling,timesteps_tested,add_reason,sticking_changed,leak_time)
                            cont = False # Do not continue with simulation at current step
                        leak_started[i] = True # Do not inject more time steps for this leak
                        
                    else:
                        data.find('Geometry/Facets')[int(leak_facet[i,0]-1)].find('Outgassing').attrib['constValue'] = "0" # Set leak rate to 0        
            
                    
         # %% Write new parameters to new xml file
        if cont:
            filename = file_to_run_name + "_iter"+str(run_iter+1)+".xml"
            write_molflow_zip_file(data,file_to_save,filename)
        
        
        # %% Run Molflow with the new parameters
        
            print('Starting Molflow simulation iteration ' + str(run_iter+1) + '/' + str(N_iter-1) + ' at ' + str(datetime.datetime.now()), flush = True)
    
            # Run MolFlow with new parameters and save results
            if reset:
                if iteration_runtime == 'By scaling factor':
                    subprocess.run([path_to_molflowCLI, "--file", file_to_save, "-o", file_to_save, "-d", str(int(Ndes_per_second/scaling_factor)), "--reset"], 
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT)
                elif iteration_runtime == 'By CPU time':
                    subprocess.run([path_to_molflowCLI, "--file", file_to_save, "-o", file_to_save, "-t", str(cpu_time_per_iter), "--reset"], 
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT)
            else:
                if iteration_runtime == 'By scaling factor':
                    subprocess.run([path_to_molflowCLI, "--file", file_to_save, "-o", file_to_save, "-d", str(int(Ndes_per_second/scaling_factor))], 
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT)
                elif iteration_runtime == 'By CPU time':
                    subprocess.run([path_to_molflowCLI, "--file", file_to_save, "-o", file_to_save, "-t", str(cpu_time_per_iter)], 
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT)
    
    
            print('Iteration ' + str(run_iter+1) + ' complete at ' + str(datetime.datetime.now()), flush = True)
        
        # %% update time steps and results and prepare for next iteration

            run_iter = run_iter+1
            if runtype != 'By number of molecules':
                timesteps_tested = np.append(timesteps_tested,timesteps[run_iter])
            
            # Save results so far, so they can be processed and plotted even if simulation is interrupted
            np.savetxt('Output/timesteps.csv', timesteps[0:run_iter], delimiter=',') # Save file to be loaded later.
            np.savetxt('Output/timesteps_tested.csv', timesteps_tested, delimiter=',') # Save file to be loaded later
            np.savetxt('Output/scaling.csv', scaling, delimiter=',') # Save file to be loaded later
            if sticking_model=='CO_physics_model':
                CO_dens = N_abs/area
                np.savetxt('Output/CO_dens.csv', CO_dens, delimiter=',') # Save file to be loaded later
                np.savetxt('Output/timesteps_CO_physics_model.csv', timesteps[0:run_iter], delimiter=',') # Save file to be loaded later.
                
        tt = tt+1        

    # %% Return results of simulation to main script
    print('Simulation finished at ' + str(datetime.datetime.now()), flush = True)
    t1 = time.time()
    print('Time elapsed for simulation: ' + str(timedelta(seconds=(t1-t0))), flush = True)
    if desorption_type == 'Dynamic PSD from Synrad data':
        print('Reminder - Make sure you check file: Output/Desorption_yield_data_and_fits.png to ensure fits to the data are reasonable!', flush=True)
    return scaling,N_abs,N_des,N_abs_before_scaling,N_des_before_scaling,timesteps[0:-1],timesteps_tested[0:-1]
