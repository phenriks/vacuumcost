# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 08:36:05 2022

@author: Peter Lindquist Henriksen
contact email 1: peter.lindquist.henriksen@cern.ch
contact email 2: peter@lindquist-henriksen.dk

"""

import numpy as np                            # Matlab like syntax for linear algebra and functions
import matplotlib.pyplot as plt                        # Plots and figures like you know them from Matlab
import sys                                             # Module to see files and folders in directories
sys.path.append('Scripts')
from Run_simulation import Run_simulation
from Continue_simulation import Continue_simulation
from Process_and_plot_results import Process_and_plot_NEG

plt.close("all")


# %% Choose simulation file
path_to_molflowCLI ="C:\Other programs\molflow_win_2.9.5_beta\molflowCLI"
save_iterations_folder = "Iterations/" # Subfolder for storing all iterations of the simulation
file_to_run_name = "Model_Molflow" # Parent model. Assumed in same directory as VacuumCOST
file_to_run_type = ".zip"

# %% Desorption type to use in simulation
desorption_types = ['Dynamic PSD from Synrad data',
                    'Constant outgassing',
                    'Constant outgassing with texture']
desorption_type = desorption_types[0] # Update desorption based on Synrad file and PSD? Requires exported texture details from Synrad.

# %% For photo-stimulated desorption
# ***** Desorption from one material *****
Material_selection_group_names = ["Cu"] # Name given in MolFlow GUI to selection groups of desorbing facets. Desorbing facets not belonging to a material selection group will throw a warning.
desorption_yield_data_files = ["desorption_yields/Groebner_CU_CO.txt"] # List the data file to use for calculating PSD

# ***** Desorption from multiple materials *****
# Material_selection_group_names = ["Cu",
#                                   "Aluminium",
#                                   "NEG"] # Names given in MolFlow GUI to selection groups for facets of each material
# desorption_yield_data_files = ["desorption_yields/Groebner_CU_CO.txt",
#                                 "desorption_yields/Jason_ALUM_CO.txt",
#                                 "desorption_yields/Jason_NEG_CO.txt"] # Corresponding data files to use for calculating PSD


# Location of exported texture area and flux data
texture_cell_area = "Texture_files/cell_areas.txt"
texture_cell_flux = "Texture_files/cell_flux.txt"

# %% Sticking models (for NEG saturation simulation)
sticking_models = ['CO_physics_model',
                   'H2_CO_dependent']
sticking_model = sticking_models[0] # Select which model to use for updating sticking coefficients.

sticking_change_calculation_type = ['Relative to initial', # Calculate change in sticking relative to the initial sticking
                                    'Relative to previous'] # Calculate change in sticking relative to the sticking in previous iteration
sticking_change_calculation = sticking_change_calculation_type[1] # How to calculate tolerable change in sticking


max_Delta_s = 0.1 # Maximum relative change in median facet sticking between each iteration. Only used for automatic temporal resolution
lower_lim_s_timeupdate = 0.05 # Lower limit of sticking (fraction of initial sticking) for when to inject extra timestep. Only used for change in sticking relative to previous
max_Delta_desorption = 0.2  # Maximum relative change in desorption between each iteration. Only used for automatic temporal resolution

# Select which facets to keep constant sticking (either by initial sticking, by facet ID, or by selection group name)
facet_inactive_critera_sticking = [0,1,0.0225] # Criteria for initial sticking for facets NOT to be considered active (to have sticking updated). Code rounds to 4 significant digits
facet_inactive_critera_ID = [] # List of facet IDs to not be considered active. Note: Facet ID as per Molflow GUI (index starting at 1)
facet_inactive_selection_group_names = ["Fixed_sticking","Pumps"] # Name given in MolFlow GUI to selection groups of facets that should be inactive during simulation.

# %% Define how to run simulation
runtypes = ['By number of molecules',
            'By physical time elapsed linear',
            'By physical time elapsed exponential',
            'By timesteps',
            'By automatic temporal resolution']
runtype = runtypes[4] # Choose to simulate a certain number of desorbed molecules or a certain physical time. Automatic or pre-defined time steps

iteration_runtime_determination = ['By CPU time', # Better control of simulation time
                                   'By scaling factor'] # Better control of statistics
iteration_runtime = iteration_runtime_determination[0]



N_iter = 24 # Used for runtypes 0,1,2. Number of iterations of simulation to go through.
runtime = 2 # Used for runtypes 1,2. If runtype linear: Physical time to run each iteration (s). If runtime exp: Each iteration runs for runtime^(n_iter) seconds so must be > 1
Ndes_to_run = 1e16 # Only used for runtype[0]. Number of molecules desorbed in each iteration.
timesteps = [1,100,1000] # Timesteps for simulation in seconds. Only used for runtype[3]
total_runtime = 1e6 # Used only for runtype[4]. Total physical time to simulate.

cpu_time_per_iter = 10 # Used for iteration_runtime_determination[0]. CPU time (s) per iteration. In this case Molflow handles the scaling factor.
scaling_factor = 1e10 # Used for iteration_runtime_determination[1]. Downscale for simulation run and upscale results to increase simulation speed (at the expensive of statistics)

# %% Add leak (facet must be defined in Molflow GUI with its leak rate
leak_facet = np.array([[]]) # If no leak 
# leak_facet = np.array([[1203,130*60],[1204,1500*60],[1205,10000*60]]) # syntax: [[leak 1 facet id in GUI, time to start leak 1],[leak 2 facet id in GUI, time to start leak 2],...]


# %% Settings for plotting - these do not affect the simulation itself.
plot_saturated = True # Choose whether to mark saturated facets with ret edges in scatter plot
saturation_limit = 0 # Enter a limit (molec/cm^2) or, or 0 to use x0 from sticking model

z_res = 1 # Spatial resolution in z-wise direction (cm). Used for binning data. Update this based on model simulated.
beam_current = 0.130 # Beam current in A. If non-zero, will add an axis showing dose in Ah along time.
keep_aspect_ratio = True # Choose to make 3D plot with true aspect ratio (1) or distorted but filling plot (0)
switch_yz = True # Choose (for 3D plotting purposes only) to switch y and z axes (making z-axis horizontal)
saturated_area_cutoff = 0 # Facets with area less than this [cm^2] are not considered saturated
saturated_fraction_cutoff = 0 # Saturation point considered to be where only this fraction of saturated area exists beyond. Only useful for linear geometries.
num_legends = 10 # Number of legends to plot - Reduce number of plotted legends in case of many iterations
plot_all_iterations = False # If not, will plot same number as num_legends
create_absorption_animation = False # Choose whether to spend time making animation or not
create_sticking_animation = False
create_pressure_animation = False

# %% Choose which scripts to run
use_previously_processed_texture_data = True # Choose whether to load and process Synrad texture data again (slow) or load previously processed arrays (fast)
run_simulation = True # Choose whether to run simulation
continue_previous = False # Continue a previous simulation. Requires run_simulation==True and runtype 3 or 4 (not necessarily same as prior simulation)
continue_from_time = 0 # Time step to continue simulation from. If 0, continue from end of previous simulation.
run_processing = True # Choose whether to run processing of simulated data
run_plotting = True # Choose whether to run plotting of processed data (requires run processing)

# %% Run simulation

# Ensure that the facets IDs to use for each desorption yield file are defined before attempting to run simulation.
if len(Material_selection_group_names)!=len(desorption_yield_data_files):
    print('Error: Make sure the no. of desorption yield data files listed matches the no. of material selection group names.')
    sys.exit()


if run_simulation:
    if continue_previous:
        timesteps_old = np.loadtxt('Output/timesteps.csv', delimiter=',')
        scaling = np.loadtxt('Output/scaling.csv', delimiter=',')
        scaling,N_abs,N_des,N_abs_before_scaling,N_des_before_scaling,timesteps,timesteps_tested = Continue_simulation(path_to_molflowCLI,save_iterations_folder,file_to_run_name,file_to_run_type,N_iter,Ndes_to_run,scaling,sticking_model,runtype,runtime,timesteps,max_Delta_s,lower_lim_s_timeupdate,facet_inactive_critera_sticking,facet_inactive_critera_ID,facet_inactive_selection_group_names,iteration_runtime,cpu_time_per_iter,scaling_factor,continue_from_time,total_runtime,timesteps_old,sticking_change_calculation,desorption_yield_data_files,Material_selection_group_names,texture_cell_flux,texture_cell_area,desorption_type,max_Delta_desorption,use_previously_processed_texture_data,leak_facet)
        np.savetxt('Output/timesteps.csv', timesteps, delimiter=',') # Save file to be loaded later
        np.savetxt('Output/timesteps_tested.csv', timesteps_tested, delimiter=',') # Save file to be loaded later
        np.savetxt('Output/scaling.csv', scaling, delimiter=',') # Save file to be loaded later

    else:
        if runtype == 'By automatic temporal resolution':
            timesteps = [total_runtime]
        scaling,N_abs,N_des,N_abs_before_scaling,N_des_before_scaling,timesteps,timesteps_tested = Run_simulation(path_to_molflowCLI,save_iterations_folder,file_to_run_name,file_to_run_type,N_iter,Ndes_to_run,sticking_model,runtype,runtime,timesteps,max_Delta_s,lower_lim_s_timeupdate,facet_inactive_critera_sticking,facet_inactive_critera_ID,facet_inactive_selection_group_names,iteration_runtime,cpu_time_per_iter,scaling_factor,sticking_change_calculation,desorption_yield_data_files,Material_selection_group_names,texture_cell_flux,texture_cell_area,desorption_type,max_Delta_desorption,use_previously_processed_texture_data,leak_facet)
        np.savetxt('Output/timesteps.csv', timesteps, delimiter=',') # Save file to be loaded later
        np.savetxt('Output/timesteps_tested.csv', timesteps_tested, delimiter=',') # Save file to be loaded later
        np.savetxt('Output/scaling.csv', scaling, delimiter=',') # Save file to be loaded later


# %% Read and plot data

if run_simulation!=1:
    N_abs = []
    N_des = []
    N_abs_before_scaling = []
    N_des_before_scaling = []

                    
if run_processing:
    timesteps = np.loadtxt('Output/timesteps.csv', delimiter=',')
    timesteps_tested = np.loadtxt('Output/timesteps_tested.csv', delimiter=',')
    scaling = np.loadtxt('Output/scaling.csv', delimiter=',')
    N_abs,N_abs_iter,N_des,s,COM,area,active_facets,saturated,saturation_point,sticking_after,P = Process_and_plot_NEG(file_to_run_name,save_iterations_folder,file_to_run_type,N_iter,Ndes_to_run,scaling,sticking_model,runtype,runtime,timesteps,plot_saturated,keep_aspect_ratio,switch_yz,N_abs,N_des,N_abs_before_scaling,N_des_before_scaling,run_simulation,saturated_area_cutoff,saturated_fraction_cutoff,run_processing,run_plotting,timesteps_tested,facet_inactive_critera_sticking,facet_inactive_critera_ID,facet_inactive_selection_group_names,saturation_limit,num_legends,plot_all_iterations,create_absorption_animation,create_sticking_animation,desorption_type,beam_current,z_res,sticking_change_calculation,create_pressure_animation,leak_facet)


# N_abs is an array of dimension (N_facets,N_iterations) with the total number of absorbed molecules by a facet n after iteration m
# N_abs_iter is an array of dimension (N_facets,N_iterations) with the number of absorbed molecules by a facet n during iteration m
# N_des is an array of dimension (N_facets,N_iterations) with the total number of desorbed molecules by a facet n after iteration m
# s is an array of dimension (N_facets,N_iterations) with the sticking coefficient for facet n during iteration m
# COM is an array of dimension (N_facets,3) with the (x,y,z)-coordiantes of the geometric center of mass of facet n
# area is an array of dimension (N_facets,1) with the area of facet n
# active_facets is a boolean array of dimension (N_facets,1) stating whether facet n is "active" (has non-zero sticking)
# saturated is a boolean array of dimension (N_facets,N_iterations) stating whether facet n is satured in iteration m
# saturation_point is an array of dimension (N_iterations,1) with the z-coordinate of the first facet that is saturated in iteration n
# P is a pressure array of dimension (N_facets,N_iterations) with the pressure for each facet after each iteration
    